# IronWorks.Structures

## Project Description

__IronWorks.Structures__ is a collection of concrete data structures (both
mutable and immutable) for working with the data of FINAL FANTASY XIV: A Realm
Reborn.

## License

__IronWorks.Structures__ is released under the MIT License ([SPDX MIT][3]). A
[markdown version][4] of the license is provided in the repository.

[3]:https://spdx.org/licenses/MIT.html
[4]:LICENSE.md
