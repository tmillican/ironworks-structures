namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///   Class representing a SqPack model data stream. Implementation of
    ///   <see cref="ISqPackDataStream"/>.
    /// </summary>
    public class SqPackModelDataStream : SqPackDataStream, ISqPackDataStream
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataStream.ContentType"/>.
        /// </summary>
        public SqPackDataStreamType ContentType
        {
            get { return SqPackDataStreamType.Model; }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.FileExtension"/>.
        /// </summary>
        public string FileExtension
        {
            get { return "mdl"; }
        }

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initializes a new instance of the <see
        ///   cref="SqPackModelDataStream"/> class with the specified
        ///   properties.
        /// </summary>
        /// <param name="uncompressedSize">
        ///   The original, uncompressed size of the data stream (file), in
        ///   bytes.
        /// </param>
        /// <param name="blocks">
        ///   A sequence of <see cref="ISqPackDataBlock"/> instances comprising
        ///   the compressed file data.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="blocks"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="uncompressedSize"/> is negative.
        /// </throws>
        public SqPackModelDataStream(
            int uncompressedSize,
            IEnumerable<ISqPackDataBlock> blocks)
            : base(uncompressedSize, blocks)
        {
        }
    }
}
