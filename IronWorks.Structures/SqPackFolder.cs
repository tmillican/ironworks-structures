namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;

    using IronWorks.Monads;

    /// <summary>
    ///   Immutable implementation of <see cref="ISqPackFolder"/>
    /// </summary>
    public partial class SqPackFolder : ISqPackFolder
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFolder.Files"/>
        /// </summary>
        public IReadOnlyList<ISqPackFile> Files
        {
            get { return _files; }
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.IsMutable"/>.
        /// </summary>
        public bool IsMutable { get { return false; } }

        /// <summary>
        ///   See <see cref="ISqPackFolder.NameHash"/>.
        /// </summary>
        public long NameHash
        {
            get { return _nameHash; }
        }

        // Fields
        // ========================================================================

        private readonly long _nameHash;

        private readonly ImmutableList<ISqPackFile> _files;

        private readonly ImmutableDictionary<long, ISqPackFile> _nameHashToFileMap;

        // Ctors
        // ========================================================================

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPackFolder"/> class
        ///   with the specified name hash, containing the specified files.
        /// </summary>
        /// <param name="nameHash">
        ///   The CRC32-SE hash of the folder name.
        /// </param>
        /// <param name="files">
        ///   An enumerable of <see cref="ISqPackFile"/> instances whose
        ///   elements should populate the <see cref="SqPackFolder"/>.
        /// </param>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="files"/> is less than <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   If any element of <paramref name="files"/> is <code>null</code>.
        /// </throws>
        public SqPackFolder(long nameHash, IEnumerable<ISqPackFile> files)
        {
            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "nameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }
            if (files == null)
            {
                throw new ArgumentNullException("files");
            }
            {
                var i = 0;
                foreach (var file in files)
                {
                    if (file == null)
                    {
                        throw new ArgumentException(
                            string.Format(
                                Strings.Argument_NullElement,
                                i),
                            "files");
                    }
                    ++i;
                }
            }

            _nameHash = nameHash;
            _files = files.ToImmutableList();
            var nameHashToFileMapBuilder
                = ImmutableDictionary.CreateBuilder<long, ISqPackFile>();
            foreach (var file in files)
            {
                nameHashToFileMapBuilder.Add(file.NameHash, file);
            }
            _nameHashToFileMap = nameHashToFileMapBuilder.ToImmutable();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPackFolder"/>
        ///   by copying another <see cref="ISqPackFolder"/> instance.
        /// </summary>
        /// <param name="other">
        ///   The instance of <see cref="ISqPackFolder"/> to copy.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="other"/> is <ocde>null</ocde>.
        /// </throws>
        public SqPackFolder(ISqPackFolder other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            _nameHash = other.NameHash;
            _files = other.Files.ToImmutableList<ISqPackFile>();
            var nameHashToFileMapBuilder
                = ImmutableDictionary.CreateBuilder<long, ISqPackFile>();
            foreach (var file in _files)
            {
                nameHashToFileMapBuilder.Add(file.NameHash, file);
            }
            _nameHashToFileMap = nameHashToFileMapBuilder.ToImmutable();
        }

        // Methods
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFolder.AsMutable()"/>.
        /// </summary>
        public IMutableSqPackFolder AsMutable()
        {
            throw new NotSupportedException(
                Strings.NotSupported_Immutable);
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.GetFileByNameHash(long)"/>.
        /// </summary>
        public Option<ISqPackFile> GetFileByNameHash(long nameHash)
        {
            return InternalGetFileByNameHash(_nameHashToFileMap, nameHash);
        }

        internal static Option<ISqPackFile> InternalGetFileByNameHash(
            IDictionary<long, ISqPackFile> map, long nameHash)
        {
            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "nameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }

            if (map.ContainsKey(nameHash))
            {
                return Option<ISqPackFile>.Some(map[nameHash]);
            }
            return Option<ISqPackFile>.None();
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.ToMutable()"/>.
        /// </summary>
        public IMutableSqPackFolder ToMutable()
        {
            return new MutableSqPackFolder(this);
        }
    }
}
