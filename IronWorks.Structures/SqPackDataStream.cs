namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;

    /// <summary>
    ///   Base class for implementations of <see cref="ISqPackDataStream"/>.
    /// </summary>
    public abstract class SqPackDataStream
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataStream.Blocks"/>.
        /// </summary>
        public IReadOnlyList<ISqPackDataBlock> Blocks
        {
            get { return _blocks; }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.IsMutable"/>.
        /// </summary>
        public bool IsMutable
        {
            get { return false; }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.OriginalHeader"/>.
        /// </summary>
        public IReadOnlyList<byte> OriginalHeader
        {
            get { return _originalHeader.ToImmutableArray(); }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.UncompressedSize"/>.
        /// </summary>
        public int UncompressedSize { get; }

        // Fields
        // =====================================================================

        private readonly ImmutableArray<ISqPackDataBlock> _blocks;

        private readonly byte[] _originalHeader;

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPackDataStream"/>
        ///   class with the specified properties.
        /// </summary>
        /// <param name="uncompressedSize">
        ///   The original, uncompressed size of the data stream (file), in
        ///   bytes.
        /// </param>
        /// <param name="originalHeader">
        ///   A sequence of bytes comprising the original, uncompressed file
        ///   header.
        /// </param>
        /// <param name="blocks">
        ///   A sequence of <see cref="ISqPackDataBlock"/> instances comprising
        ///   the compressed file data.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="blocks"/> <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="blocks"/> or <paramref name="originalHeader"/> is
        ///   <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="uncompressedSize"/> is negative.
        /// </throws>
        public SqPackDataStream(
            int uncompressedSize,
            IEnumerable<byte> originalHeader,
            IEnumerable<ISqPackDataBlock> blocks)
            : this(uncompressedSize, blocks)
        {
            if (originalHeader is null)
            {
                throw new ArgumentNullException(nameof(originalHeader));
            }

            _originalHeader = originalHeader.ToArray();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPackDataStream"/>
        ///   class with the specified properties.
        /// </summary>
        /// <param name="uncompressedSize">
        ///   The original, uncompressed size of the data stream (file), in
        ///   bytes.
        /// </param>
        /// <param name="blocks">
        ///   A sequence of <see cref="ISqPackDataBlock"/> instances comprising
        ///   the compressed file data.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="blocks"/> <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="blocks"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="uncompressedSize"/> is negative.
        /// </throws>
        protected SqPackDataStream(
            int uncompressedSize,
            IEnumerable<ISqPackDataBlock> blocks)
        {
            if (blocks is null)
            {
                throw new ArgumentNullException(nameof(blocks));
            }
            {
                var i = 0;
                foreach (var block in blocks)
                {
                    if (block is null)
                    {
                        throw new ArgumentException(
                            string.Format(
                                Strings.Argument_NullElement, i),
                            nameof(blocks));
                    }
                    ++i;
                }
            }
            if (uncompressedSize < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(uncompressedSize),
                    Strings.ArgumentOutOfRange_NegativeSize);
            }

            UncompressedSize = uncompressedSize;
            _blocks = blocks.ToImmutableArray();
        }

        // Methods
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataStream.Unpack()"/>.
        /// </summary>
        public byte[] Unpack()
        {
            var outStream = new MemoryStream();
            Unpack(outStream);
            return outStream.ToArray();
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.Unpack(Stream)"/>.
        /// </summary>
        public void Unpack(Stream outStream)
        {
            if (!(_originalHeader is null))
            {
                outStream.Write(_originalHeader, 0, _originalHeader.Length);
            }
            foreach (var block in _blocks)
            {
                block.Unpack(outStream);
            }
        }
    }
}
