namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;

    /// <summary>
    ///   Implementation of <see cref="ISqPackDataBlock"/>.
    /// </summary>
    public class SqPackDataBlock : ISqPackDataBlock
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataBlock.IsCompressed"/>.
        /// </summary>
        public bool IsCompressed { get; }

        /// <summary>
        ///   See <see cref="ISqPackDataBlock.IsMutable"/>.
        /// </summary>
        public bool IsMutable { get { return false; } }

        /// <summary>
        ///   See <see cref="ISqPackDataBlock.Data"/>.
        /// </summary>
        public IReadOnlyList<byte> Data {
            get { return _data.ToImmutableList(); }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataBlock.UncompressedSize"/>.
        /// </summary>
        public int UncompressedSize { get; }

        // Fields
        // =====================================================================

        private byte[] _data;

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initialize a new instance of the <see cref="ISqPackDataBlock"/>
        ///   class with the specified properties.
        /// </summary>
        /// <param name="isCompressed">
        ///   A value indicating whether the block is compressed.
        /// </param>
        /// <param name="uncompressedSize">
        ///   The uncompressed size of the block, in bytes.
        /// </param>
        /// <param name="data">
        ///   A sequence of bytes comprising the block data.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="data"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="uncompressedSize"/> is negative.
        /// </throws>
        public SqPackDataBlock(
            bool isCompressed,
            int uncompressedSize,
            IEnumerable<byte> data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            if (uncompressedSize < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(uncompressedSize),
                    Strings.ArgumentOutOfRange_NegativeSize);
            }

            IsCompressed = isCompressed;
            UncompressedSize = uncompressedSize;
            _data = data.ToArray();
        }

        // Methods
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataBlock.Unpack()"/>.
        /// </summary>
        public byte[] Unpack()
        {
            var outStream = new MemoryStream();
            Unpack(outStream);
            return outStream.ToArray();
        }

        /// <summary>
        ///   See <see cref="ISqPackDataBlock.Unpack(Stream)"/>.
        /// </summary>
        public void Unpack(Stream outStream)
        {
            if (!IsCompressed)
            {
                outStream.Write(_data, 0, _data.Length);
                return;
            }

            using (var inStream = new MemoryStream(_data))
            {
                using (var deflateStream = new DeflateStream(
                           inStream, CompressionMode.Decompress))
                {
                    deflateStream.CopyTo(outStream);
                }
            }
        }
    }
}
