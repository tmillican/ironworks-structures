namespace IronWorks.Structures
{
    using System.Collections.Generic;

    public partial class SqPackFolder : ISqPackFolder
    {
        /// <summary>
        ///   Builder for <see cref="SqPackFolder"/>
        /// </summary>
        public class Builder : MutableSqPackFolder
        {
            /// <summary>
            ///   See <see cref="MutableSqPackFolder(long)"/>.
            /// </summary>
            public Builder(long nameHash)
                : base(nameHash)
            {
            }

            /// <summary>
            ///   See <see cref="MutableSqPackFolder(long,
            ///   IEnumerable{ISqPackFile})"/>.
            /// </summary>
            public Builder(long nameHash, IEnumerable<ISqPackFile> files)
                : base(nameHash, files)
            {
            }
        }
    }
}
