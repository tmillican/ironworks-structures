namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///   Class representing a SqPack texture data stream. Implementation of
    ///   <see cref="ISqPackDataStream"/>.
    /// </summary>
    public class SqPackTextureDataStream : SqPackDataStream, ISqPackDataStream
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataStream.ContentType"/>.
        /// </summary>
        public SqPackDataStreamType ContentType
        {
            get { return SqPackDataStreamType.Texture; }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.FileExtension"/>.
        /// </summary>
        public string FileExtension
        {
            get { return "tex"; }
        }

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initializes a new instance of the <see
        ///   cref="SqPackTextureDataStream"/> class with the specified
        ///   properties.
        /// </summary>
        /// <param name="uncompressedSize">
        ///   The original, uncompressed size of the data stream (file), in
        ///   bytes.
        /// </param>
        /// <param name="originalHeader">
        ///   A sequence of bytes comprising the original, uncompressed file
        ///   header.
        /// </param>
        /// <param name="blocks">
        ///   A sequence of <see cref="ISqPackDataBlock"/> instances comprising
        ///   the compressed file data.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="blocks"/> or <paramref name="originalHeader"/> is
        ///   <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="uncompressedSize"/> is negative.
        /// </throws>
        public SqPackTextureDataStream(
            int uncompressedSize,
            IEnumerable<byte> originalHeader,
            IEnumerable<ISqPackDataBlock> blocks)
            : base(uncompressedSize, originalHeader, blocks)
        {
        }
    }
}
