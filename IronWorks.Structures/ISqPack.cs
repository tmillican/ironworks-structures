namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using IronWorks.Monads;

    /// <summary>
    ///   Interface representing an SqPack archive
    /// </summary>
    public interface ISqPack
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   Gets a <see cref="DirectoryInfo"/> object identifying the
        ///   directory that contains the SqPack archive's files (.index and
        ///   .dat files).
        /// </summary>
        /// <value>
        ///   A <see cref="DirectoryInfo"/> object identifying the directory
        ///   that contains the SqPack archive's files (.index and .dat files).
        /// </value>
        DirectoryInfo Directory { get; }

        /// <summary>
        ///   Gets a string containing the file name root of the SqPack archive.
        /// </summary>
        /// <value>
        ///   A string containing the file name root of the SqPack archive.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     For example, an archive with index file name of
        ///     "/users/john.doe/my_archive.index" would have a file name root
        ///     of "my_archive".
        ///   </para>
        /// </remarks>
        string FileNameRoot { get; }

        /// <summary>
        ///   Gets a list of <see cref="ISqPackFolder"/> instances representing
        ///   the folders in the <see cref="ISqPack"/>.
        /// </summary>
        /// <value>
        ///   A list of <see cref="ISqPackFolder"/> instances representing
        ///   the folders in the <see cref="ISqPack"/>.
        /// </value>
        IReadOnlyList<ISqPackFolder> Folders { get; }

        /// <summary>
        ///   Gets a value indicating whether this <see cref="ISqPack"/>
        ///   instance is mutable.
        /// </summary>
        /// <value>
        ///   If this instance is mutable, <code>true</code>. Otherwise
        ///   <code>false</code>.
        /// </value>
        bool IsMutable { get; }

        // Methods
        // ========================================================================

        /// <summary>
        ///   Casts this <see cref="ISqPack"/> to an <see
        ///   cref="IMutableSqPack"/> instance.
        /// </summary>
        /// <throws cref="NotSupportedException">
        ///   <see cref="IsMutable"/> is <code>false</code>.
        /// </throws>
        /// <returns>
        ///   A cast of this <see cref="ISqPack"/> to <see
        ///   cref="IMutableSqPack"/>.
        /// </returns>
        IMutableSqPack AsMutable();

        /// <summary>
        ///   Gets the <see cref="ISqPackFolder"/> instance in the <see
        ///   cref="ISqPack"/> identified by <paramref name="nameHash"/>.
        /// </summary>
        /// <value>
        ///   A <code>Option&lt;ISqPackFolder&gt;</code> object containing the
        ///   result of the get.
        /// </value>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        Option<ISqPackFolder> GetFolderByNameHash(long nameHash);

        /// <summary>
        ///   Gets the <see cref="ISqPackFile"/> instance in the <see
        ///   cref="ISqPack"/> at the path identified by the <paramref
        ///   name="folderNameHash"/> and <paramref name="fileNameHash"/>.
        /// </summary>
        /// <value>
        ///   A <code>Option&lt;ISqPackFile&gt;</code> object containing the
        ///   result of the get.
        /// </value>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="folderNameHash"/> is less than <code>0</code> or
        ///   greater than <code>UInt32.MaxValue</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="fileNameHash"/> is less than <code>0</code> or
        ///   greater than <code>UInt32.MaxValue</code>.
        /// </throws>
        Option<ISqPackFile> GetFileByPathNameHash(
            long folderNameHash, long fileNameHash);

        /// <summary>
        ///   Produces a mutable copy of this <see cref="ISqPack"/>.
        /// </summary>
        /// <returns>
        ///   A mutable copy of this <see cref="ISqPack"/>.
        /// </returns>
        /// <remarks>
        ///   <para>
        ///     If <see cref="IsMutable"/> is <code>true</code>, you may wish to
        ///     avoid the copy by casting the instance with <see
        ///     cref="AsMutable()"/>.
        ///   </para>
        /// </remarks>
        IMutableSqPack ToMutable();
    }
}
