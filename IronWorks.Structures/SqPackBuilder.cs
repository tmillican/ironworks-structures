namespace IronWorks.Structures
{
    using System.Collections.Generic;
    using System.IO;

    public partial class SqPack : ISqPack
    {
        /// <summary>
        ///   Builder for <see cref="SqPack"/>
        /// </summary>
        public class Builder : MutableSqPack
        {
            /// <summary>
            ///   See <see cref="MutableSqPack(DirectoryInfo, string)"/>.
            /// </summary>
            public Builder(DirectoryInfo directory, string fileNameRoot)
                : base(directory, fileNameRoot)
            {
            }

            /// <summary>
            ///   See <see cref="MutableSqPack(DirectoryInfo, string,
            ///   IEnumerable{ISqPackFolder})"/>.
            /// </summary>
            public Builder(
                DirectoryInfo directory,
                string fileNameRoot,
                IEnumerable<ISqPackFolder> folders)
                : base(directory, fileNameRoot, folders)
            {
            }
        }
    }
}
