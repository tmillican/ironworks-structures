namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.IO;
    using System.Linq;

    using IronWorks.Monads;

    /// <summary>
    ///   Implementation of <see cref="ISqPack"/>.
    /// </summary>
    public partial class SqPack : ISqPack
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPack.Directory"/>.
        /// </summary>
        public DirectoryInfo Directory { get { return _directory; } }

        /// <summary>
        ///   See <see cref="ISqPack.FileNameRoot"/>.
        /// </summary>
        public string FileNameRoot { get { return _fileNameRoot; } }

        /// <summary>
        ///   See <see cref="ISqPack.Folders"/>.
        /// </summary>
        public IReadOnlyList<ISqPackFolder> Folders { get { return _folders; } }

        /// <summary>
        ///   See <see cref="ISqPack.IsMutable"/>.
        /// </summary>
        public bool IsMutable { get { return false; } }

        // Fields
        // ========================================================================

        private readonly DirectoryInfo _directory;

        private readonly string _fileNameRoot;

        private readonly ImmutableList<ISqPackFolder> _folders;

        private readonly
            ImmutableDictionary<long, ISqPackFolder> _nameHashToFolderMap;

        // Ctors
        // ========================================================================

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPack"/>
        ///   class with the specified directory, file name root, and set
        ///   folders.
        /// </summary>
        /// <param name="directory">
        ///   A <see cref="DirectoryInfo"/> object identifying the directory
        ///   that contains the SqPack archive's files (.index and .dat files).
        /// </param>
        /// <param name="fileNameRoot">
        ///   A string containing the file name root of the SqPack archive.
        /// </param>
        /// <param name="folders">
        ///   A sequence of <see cref="ISqPackFolder"/> instances whose elements
        ///   will populate the <see cref="ISqPack"/>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="directory"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="fileNameRoot"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="folders"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   If any element of <paramref name="folders"/> is <code>null</code>.
        /// </throws>
        public SqPack(
            DirectoryInfo directory,
            string fileNameRoot,
            IEnumerable<ISqPackFolder> folders)
        {
            if (directory == null)
            {
                throw new ArgumentNullException("directory");
            }
            if (fileNameRoot == null)
            {
                throw new ArgumentNullException("fileNameRoot");
            }
            if (folders == null)
            {
                throw new ArgumentNullException("folders");
            }
            {
                var i = 0;
                foreach (var folder in folders)
                {
                    if (folder == null)
                    {
                        throw new ArgumentException(
                            string.Format(Strings.Argument_NullElement, i),
                            "folders");
                    }
                    ++i;
                }
            }

            _directory = directory;
            _fileNameRoot = fileNameRoot;
            _folders = folders.ToImmutableList();

            var nameHashToFolderMapBuilder
                = ImmutableDictionary.CreateBuilder<long, ISqPackFolder>();
            foreach (var folder in folders)
            {
                nameHashToFolderMapBuilder.Add(folder.NameHash, folder);
            }
            _nameHashToFolderMap = nameHashToFolderMapBuilder.ToImmutable();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPack"/>
        ///   by copying another <see cref="ISqPack"/> instance.
        /// </summary>
        /// <param name="other">
        ///   The instance of <see cref="ISqPack"/> to copy.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="other"/> is <ocde>null</ocde>.
        /// </throws>
        public SqPack(ISqPack other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            _directory = new DirectoryInfo(other.Directory.FullName);
            _fileNameRoot = other.FileNameRoot;
            _folders = other.Folders.ToImmutableList();
            var nameHashToFolderMapBuilder
                = ImmutableDictionary.CreateBuilder<long, ISqPackFolder>();
            foreach (var folder in _folders)
            {
                nameHashToFolderMapBuilder.Add(folder.NameHash, folder);
            }
            _nameHashToFolderMap = nameHashToFolderMapBuilder.ToImmutable();
        }

        // Methods
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPack.AsMutable()"/>.
        /// </summary>
        public IMutableSqPack AsMutable()
        {
            throw new NotSupportedException(
                Strings.NotSupported_Immutable);
        }

        /// <summary>
        ///   See <see cref="ISqPack.GetFolderByNameHash(long)"/>.
        /// </summary>
        public Option<ISqPackFolder> GetFolderByNameHash(long nameHash)
        {
            return InternalGetFolderByNameHash(
                _nameHashToFolderMap,
                nameHash);
        }

        /// <summary>
        ///   See <see cref="ISqPack.GetFileByPathNameHash(long, long)"/>.
        /// </summary>
        public Option<ISqPackFile> GetFileByPathNameHash(
            long folderNameHash,
            long fileNameHash)
        {
            return InternalGetFileByPathNameHash(
                _nameHashToFolderMap,
                folderNameHash,
                fileNameHash);
        }

        /// <summary>
        ///   See <see cref="ISqPack.GetFolderByNameHash(long)"/>.
        /// </summary>
        internal static Option<ISqPackFolder> InternalGetFolderByNameHash(
            IDictionary<long, ISqPackFolder> map,
            long nameHash)
        {
            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "nameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }

            if (map.ContainsKey(nameHash))
            {
                return Option<ISqPackFolder>.Some(map[nameHash]);
            }
            return Option<ISqPackFolder>.None();
        }

        /// <summary>
        ///   See <see cref="ISqPack.GetFileByPathNameHash(long, long)"/>.
        /// </summary>
        internal static Option<ISqPackFile> InternalGetFileByPathNameHash(
            IDictionary<long, ISqPackFolder> map,
            long folderNameHash,
            long fileNameHash)
        {
            if (folderNameHash < 0 || folderNameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "folderNameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }
            if (fileNameHash < 0 || fileNameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "fileNameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }

            if (map.ContainsKey(folderNameHash))
            {
                var folder = map[folderNameHash];
                return folder.GetFileByNameHash(fileNameHash);
            }
            return Option<ISqPackFile>.None();
        }

        /// <summary>
        ///   See <see cref="ISqPack.ToMutable()"/>.
        /// </summary>
        public IMutableSqPack ToMutable()
        {
            return new MutableSqPack(this);
        }
    }
}
