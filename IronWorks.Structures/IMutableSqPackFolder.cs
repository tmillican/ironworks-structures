namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;

    using IronWorks.Monads;

    /// <summary>
    ///   Interface representing a mutable folder stored in an SqPack archive.
    /// </summary>
    public interface IMutableSqPackFolder : ISqPackFolder
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   Gets or sets the CRC-32SE hash of the folder name.
        /// </summary>
        /// <value>
        ///   The CRC-32SE hash of the folder name.
        /// </value>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   The property is set and <paramref name="value"/> is less than
        ///   <code>0</code> or greater than <code>UInt32.MaxValue</code>.
        /// </throws>
        new long NameHash { get; set; }

        // Methods
        // ========================================================================

        /// <summary>
        ///   Adds a file to the <see cref="IMutableSqPackFolder"/>.
        /// </summary>
        /// <param name="file">
        ///   An <see cref="ISqPackFile"/> instance representing the file.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   <paramref name="file"/> duplicates the <see
        ///   cref="ISqPackFile.NameHash"/> property of any existing file in the
        ///   <see cref="IMutableSqPackFolder"/>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="file"/> is <code>null</code>.
        /// </throws>
        void Add(ISqPackFile file);

        /// <summary>
        ///   Adds a range of files to the <see cref="IMutableSqPackFolder"/>.
        /// </summary>
        /// <param name="files">
        ///   An enumerable of <see cref="ISqPackFile"/> instances representing
        ///   the files.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="files"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="files"/> duplicates the <see
        ///   cref="ISqPackFile.NameHash"/> property of any existing file in the
        ///   <see cref="IMutableSqPackFolder"/>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="files"/> duplicates the <see
        ///   cref="ISqPackFile.NameHash"/> property of any other element in
        ///   <paramref name="files"/>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="files"/> is <code>null</code>.
        /// </throws>
        void AddRange(IEnumerable<ISqPackFile> files);

        /// <summary>
        ///   Removes the specified <see cref="ISqPackFile"/> instance from the
        ///   <see cref="IMutableSqPackFolder"/>.
        /// </summary>
        /// <throws cref="ArgumentException">
        ///   The <see cref="IMutableSqPackFolder"/> does not contain <paramref
        ///   name="file"/>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="file"/> is <code>null</code>.
        /// </throws>
        void Remove(ISqPackFile file);

        /// <summary>
        ///   Produces an immutable copy of this <see cref="IMutableSqPackFolder"/>.
        /// </summary>
        /// <returns>
        ///   An immutable copy of this <see cref="IMutableSqPackFolder"/>.
        /// </returns>
        ISqPackFolder ToImmutable();
    }
}
