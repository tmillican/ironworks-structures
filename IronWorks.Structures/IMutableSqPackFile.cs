namespace IronWorks.Structures
{
    using System;

    /// <summary>
    ///   Interface representing a mutable file stored in an SqPack archive.
    /// </summary>
    public interface IMutableSqPackFile : ISqPackFile
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   Gets or sets the offset to the file's data stream in its data
        ///   volume.
        /// </summary>
        /// <value>
        ///   The offset, in bytes, to the file's data stream, relative to the
        ///   beginning of its data volume.
        /// </value>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   The property is set and <paramref name="value"/> is negative.
        /// </throws>
        /// <throws cref="NotSupportedException">
        ///   The property is set and the <see cref="ISqPackFile"/> is
        ///   read-only.
        /// </throws>
        new long DataOffset { get; set; }

        /// <summary>
        ///   Gets or sets a number indicating which data volume (.dat file )
        ///   contains the file's data.
        /// </summary>
        /// <value>
        ///   A number indicating which data volume (.dat file ) contains the
        ///   file's data.
        /// </value>
        /// <throws cref="NotSupportedException">
        ///   The property is set and the <see cref="ISqPackFile"/> is
        ///   read-only.
        /// </throws>
        new byte DataVolumeId { get; set; }

        /// <summary>
        ///   Gets or sets the CRC-32SE hash of the file name.
        /// </summary>
        /// <value>
        ///   The CRC-32SE hash of the file name.
        /// </value>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   The property is set and <paramref name="value"/> is less than
        ///   <code>0</code> or greater than <code>UInt32.MaxValue</code>.
        /// </throws>
        /// <throws cref="NotSupportedException">
        ///   The property is set and the <see cref="ISqPackFile"/> is
        ///   read-only.
        /// </throws>
        new long NameHash { get; set; }

        // Methods
        // ========================================================================

        /// <summary>
        ///   Produces an immutable copy of this <see cref="IMutableSqPackFile"/>.
        /// </summary>
        /// <returns>
        ///   An immutable copy of this <see cref="IMutableSqPackFile"/>.
        /// </returns>
        ISqPackFile ToImmutable();
    }
}
