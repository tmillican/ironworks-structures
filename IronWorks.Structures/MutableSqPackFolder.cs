namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;

    using IronWorks.Monads;

    /// <summary>
    ///   Implementation of <see cref="IMutableSqPackFolder"/>
    /// </summary>
    public class MutableSqPackFolder : IMutableSqPackFolder
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFolder.Files"/>.
        /// </summary>
        public IReadOnlyList<ISqPackFile> Files
        {
            get { return _files.ToImmutableList<ISqPackFile>(); }
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.IsMutable"/>.
        /// </summary>
        public bool IsMutable { get { return true; } }

        /// <summary>
        ///   See <see cref="IMutableSqPackFolder.NameHash"/>.
        /// </summary>
        public long NameHash
        {
            get { return _nameHash; }
            set
            {
                if (value < 0 || value > uint.MaxValue)
                {
                    throw new ArgumentOutOfRangeException(
                        "value",
                        string.Format(
                            Strings.ArgumentOutOfRange_Range,
                            0,
                            uint.MaxValue));
                }
                _nameHash = value;
            }
        }

        // Fields
        // ========================================================================

        private long _nameHash;

        private List<ISqPackFile> _files;

        private Dictionary<long, ISqPackFile> _nameHashToFileMap;

        // Ctors
        // ========================================================================

        /// <summary>
        ///   Initializes a new, empty instance of the <see
        ///   cref="MutableSqPackFolder"/> class with the specified name hash.
        /// </summary>
        /// <param name="nameHash">
        ///   The CRC32-SE hash of the folder name.
        /// </param>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        public MutableSqPackFolder(long nameHash)
            : this(nameHash, new ISqPackFile[0])
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see
        ///   cref="MutableSqPackFolder"/> class with the specified name hash,
        ///   containing the specified files.
        /// </summary>
        /// <param name="nameHash">
        ///   The CRC32-SE hash of the folder name.
        /// </param>
        /// <param name="files">
        ///   An enumerable of <see cref="ISqPackFile"/> instances whose
        ///   elements should populate the <see cref="MutableSqPackFolder"/>.
        /// </param>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="files"/> is less than <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   If any element of <paramref name="files"/> is <code>null</code>.
        /// </throws>
        public MutableSqPackFolder(
            long nameHash,
            IEnumerable<ISqPackFile> files)
        {
            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "nameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }
            if (files == null)
            {
                throw new ArgumentNullException("files");
            }
            {
                var i = 0;
                foreach (var file in files)
                {
                    if (file == null)
                    {
                        throw new ArgumentException(
                            string.Format(
                                Strings.Argument_NullElement,
                                i),
                            "files");
                    }
                    ++i;
                }
            }

            _nameHash = nameHash;
            _files = files.ToList();
            _nameHashToFileMap
                = new Dictionary<long, ISqPackFile>(_files.Count);
            foreach (var file in files)
            {
                _nameHashToFileMap.Add(file.NameHash, file);
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="MutableSqPackFolder"/>
        ///   by copying another <see cref="ISqPackFolder"/> instance.
        /// </summary>
        /// <param name="other">
        ///   The instance of <see cref="ISqPackFolder"/> to copy.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="other"/> is <ocde>null</ocde>.
        /// </throws>
        public MutableSqPackFolder(ISqPackFolder other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            _nameHash = other.NameHash;
            _files = other.Files.ToList();
            _nameHashToFileMap
                = new Dictionary<long, ISqPackFile>(_files.Count);
            foreach (var file in _files)
            {
                _nameHashToFileMap.Add(file.NameHash, file);
            }
        }

        // Methods
        // ========================================================================

        /// <summary>
        ///   See <see cref="IMutableSqPackFolder.Add(ISqPackFile)"/>.
        /// </summary>
        public void Add(ISqPackFile file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            if (_nameHashToFileMap.ContainsKey(file.NameHash))
            {
                var otherFile = _nameHashToFileMap[file.NameHash];
                if (file == otherFile)
                {
                    throw new ArgumentException(
                        Strings.Argument_DuplicatesExistingFile,
                        "file");
                }
                throw new ArgumentException(
                    Strings.Argument_DuplicatesExistingFileNameHash,
                    "file");
            }

            _files.Add(file);
            _nameHashToFileMap.Add(file.NameHash, file);
        }

        /// <summary>
        ///   See <see
        ///   cref="IMutableSqPackFolder.AddRange(IEnumerable{ISqPackFile})"/>.
        /// </summary>
        public void AddRange(IEnumerable<ISqPackFile> files)
        {
            PrivateCheckAddRangeParams(files);

            _files.AddRange(files);
            foreach (var file in files)
            {
                _nameHashToFileMap.Add(file.NameHash, file);
            }
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.AsMutable()"/>.
        /// </summary>
        public IMutableSqPackFolder AsMutable()
        {
            return this;
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.GetFileByNameHash(long)"/>.
        /// </summary>
        public Option<ISqPackFile> GetFileByNameHash(long nameHash)
        {
            return SqPackFolder
                .InternalGetFileByNameHash(_nameHashToFileMap, nameHash);
        }

        private void PrivateCheckAddRangeParams(IEnumerable<ISqPackFile> files)
        {
            if (files == null)
            {
                throw new ArgumentNullException("files");
            }

            // Construct an index-tagged list of the files. We need the index to
            // report which element of "files" is at fault.
            var fileList = new List<(int index, ISqPackFile file)>();
            {
                var i = 0;
                foreach (var file in files)
                {
                    fileList.Add((i, file));
                    ++i;
                }
            }

            // Check for null elements.
            var nullFiles =
                from fileTuple in fileList
                where fileTuple.file == null
                select fileTuple;
            if (nullFiles.Any())
            {
                var nullIndex = nullFiles.First().index;
                throw new ArgumentException(
                    string.Format(Strings.Argument_NullElement, nullIndex),
                    "files");
            }

            // Check for internal duplicates. That is, an element that repeats
            // the NameHash of some previous element.
            var internalDuplicates =
                from fileTuple in fileList
                group fileTuple by fileTuple.file.NameHash into filesGroupedByNameHash
                // Select any groups such that there are any elements in the group
                // after skipping the first (i.e. Count() > 1)
                where filesGroupedByNameHash.Skip(1).Any()
                from fileTuple in filesGroupedByNameHash
                select fileTuple;
            if (internalDuplicates.Any())
            {
                var dupIdx1 = internalDuplicates.First().index;
                var dupIdx2 = internalDuplicates.Skip(1).First().index;
                throw new ArgumentException(
                    string.Format(
                        Strings.Argument_ElementDuplicatesPreviousNameHash,
                        dupIdx2,
                        dupIdx1),
                    "files");
            }

            // Check for external duplicates. That is, an element that repeats
            // the NameHash of a file already in the folder.
            var duplicateFiles =
                from fileTuple in fileList
                where _nameHashToFileMap.ContainsKey(fileTuple.file.NameHash)
                select fileTuple;
            if (duplicateFiles.Any())
            {
                // Distinguish between true duplicate files and different file
                // objects with the same namehash for more useful error
                // reporting.
                var (duplicateIdx, file) = duplicateFiles.First();
                if (_nameHashToFileMap[file.NameHash] == file)
                {
                    throw new ArgumentException(
                        string.Format(
                            Strings.Argument_ElementDuplicatesExistingFile,
                            duplicateIdx),
                        "files");
                }
                throw new ArgumentException(
                    string.Format(
                        Strings.Argument_ElementDuplicatesExistingFileNameHash,
                        duplicateIdx),
                    "files");
            }
        }

        /// <summary>
        ///   See <see cref="IMutableSqPackFolder.Remove(ISqPackFile)"/>.
        /// </summary>
        public void Remove(ISqPackFile file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            if (!_nameHashToFileMap.ContainsKey(file.NameHash))
            {
                throw new ArgumentException(
                    Strings.Argument_NonExistentFile,
                    "file");
            }

            _files.Remove(file);
            _nameHashToFileMap.Remove(file.NameHash);
        }

        /// <summary>
        ///   See <see cref="ISqPackFolder.ToMutable()"/>.
        /// </summary>
        public IMutableSqPackFolder ToMutable()
        {
            return new MutableSqPackFolder(this);
        }

        /// <summary>
        ///   See <see cref="IMutableSqPackFolder.ToImmutable()"/>.
        /// </summary>
        public ISqPackFolder ToImmutable()
        {
            return new SqPackFolder(this);
        }
    }
}
