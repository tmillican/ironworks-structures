namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///   Class representing a SqPack binary data stream. Implementation of
    ///   <see cref="ISqPackDataStream"/>.
    /// </summary>
    public class SqPackBinaryDataStream : SqPackDataStream, ISqPackDataStream
    {
        // Properties
        // =====================================================================

        /// <summary>
        ///   See <see cref="ISqPackDataStream.ContentType"/>.
        /// </summary>
        public SqPackDataStreamType ContentType
        {
            get { return SqPackDataStreamType.Binary; }
        }

        /// <summary>
        ///   See <see cref="ISqPackDataStream.FileExtension"/>.
        /// </summary>
        public string FileExtension
        {
            get { return "bin"; }
        }

        // Ctors
        // =====================================================================

        /// <summary>
        ///   Initializes a new instance of the <see
        ///   cref="SqPackBinaryDataStream"/> class with the specified
        ///   properties.
        /// </summary>
        /// <param name="uncompressedSize">
        ///   The original, uncompressed size of the data stream (file), in
        ///   bytes.
        /// </param>
        /// <param name="blocks">
        ///   A sequence of <see cref="ISqPackDataBlock"/> instances comprising
        ///   the compressed file data.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="blocks"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="uncompressedSize"/> is negative.
        /// </throws>
        public SqPackBinaryDataStream(
            int uncompressedSize,
            IEnumerable<ISqPackDataBlock> blocks)
            : base(uncompressedSize, blocks)
        {
        }
    }
}
