namespace IronWorks.Structures
{
    using System;

    /// <summary>
    ///   Interface representing a file stored in an SqPack archive.
    /// </summary>
    public interface ISqPackFile
    {
        /// <summary>
        ///   Gets the offset to the file's data stream in its data volume.
        /// </summary>
        /// <value>
        ///   The offset, in bytes, to the file's data stream, relative to the
        ///   beginning of its data volume.
        /// </value>
        long DataOffset { get; }

        /// <summary>
        ///   Gets a number indicating which data volume (.dat file ) contains
        ///   the file's data.
        /// </summary>
        /// <value>
        ///   A number indicating which data volume (.dat file ) contains the
        ///   file's data.
        /// </value>
        byte DataVolumeId { get; }

        /// <summary>
        ///   Gets a value indicating whether this <see cref="ISqPackFile"/>
        ///   instance is mutable.
        /// </summary>
        /// <value>
        ///   If this instance is mutable, <code>true</code>. Otherwise
        ///   <code>false</code>.
        /// </value>
        bool IsMutable { get; }

        /// <summary>
        ///   Gets the CRC-32SE hash of the file name.
        /// </summary>
        /// <value>
        ///   The CRC-32SE hash of the file name.
        /// </value>
        long NameHash { get; }

        // Methods
        // ========================================================================

        /// <summary>
        ///   Produces a mutable copy of this <see cref="ISqPackFile"/>.
        /// </summary>
        /// <returns>
        ///   A mutable copy of this <see cref="ISqPackFile"/>.
        /// </returns>
        /// <remarks>
        ///   <para>
        ///     If <see cref="IsMutable"/> is <code>true</code>, you may wish to
        ///     avoid the copy by casting the instance with <see
        ///     cref="AsMutable()"/>.
        ///   </para>
        /// </remarks>
        IMutableSqPackFile ToMutable();

        /// <summary>
        ///   Casts this <see cref="ISqPackFile"/> to an <see
        ///   cref="IMutableSqPackFile"/> instance.
        /// </summary>
        /// <throws cref="NotSupportedException">
        ///   <see cref="IsMutable"/> is <code>false</code>.
        /// </throws>
        /// <returns>
        ///   A cast of this <see cref="ISqPackFile"/> to <see
        ///   cref="IMutableSqPackFile"/>.
        /// </returns>
        IMutableSqPackFile AsMutable();
    }
}
