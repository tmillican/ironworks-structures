namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.IO;
    using System.Linq;

    using IronWorks.Monads;

    /// <summary>
    ///   Implementation of <see cref="IMutableSqPack"/>.
    /// </summary>
    public class MutableSqPack : IMutableSqPack
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   See <see cref="IMutableSqPack.Directory"/>.
        /// </summary>
        public DirectoryInfo Directory {
            get { return _directory; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _directory = value;
            }
        }

        /// <summary>
        ///   See <see cref="IMutableSqPack.FileNameRoot"/>.
        /// </summary>
        public string FileNameRoot
        {
            get { return _fileNameRoot; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _fileNameRoot = value;
            }
        }

        /// <summary>
        ///   See <see cref="ISqPack.Folders"/>.
        /// </summary>
        public IReadOnlyList<ISqPackFolder> Folders
        {
            get { return _folders.ToImmutableList<ISqPackFolder>(); }
        }

        /// <summary>
        ///   See <see cref="ISqPack.IsMutable"/>.
        /// </summary>
        public bool IsMutable { get { return true; } }

        // Fields
        // ========================================================================

        private DirectoryInfo _directory;

        private string _fileNameRoot;

        private List<ISqPackFolder> _folders;

        private Dictionary<long, ISqPackFolder> _nameHashToFolderMap;

        // Ctors
        // ========================================================================

        /// <summary>
        ///   Initializes a new, empty instance of the <see
        ///   cref="MutableSqPack"/> class with the specified directory and file
        ///   name root.
        /// </summary>
        /// <param name="directory">
        ///   A <see cref="DirectoryInfo"/> object identifying the directory
        ///   that contains the SqPack archive's files (.index and .dat files).
        /// </param>
        /// <param name="fileNameRoot">
        ///   A string containing the file name root of the SqPack archive.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="directory"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="fileNameRoot"/> is <code>null</code>.
        /// </throws>
        public MutableSqPack(DirectoryInfo directory, string fileNameRoot)
            : this(directory, fileNameRoot, new List<ISqPackFolder>())
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="MutableSqPack"/>
        ///   class with the specified directory, file name root, and set
        ///   folders.
        /// </summary>
        /// <param name="directory">
        ///   A <see cref="DirectoryInfo"/> object identifying the directory
        ///   that contains the SqPack archive's files (.index and .dat files).
        /// </param>
        /// <param name="fileNameRoot">
        ///   A string containing the file name root of the SqPack archive.
        /// </param>
        /// <param name="folders">
        ///   An enumerable of <see cref="ISqPackFolder"/> instances whose
        ///   elements will populate the <see cref="ISqPack"/>.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="directory"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="fileNameRoot"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="folders"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   If any element of <paramref name="folders"/> is <code>null</code>.
        /// </throws>
        public MutableSqPack(
            DirectoryInfo directory,
            string fileNameRoot,
            IEnumerable<ISqPackFolder> folders)
        {
            if (directory == null)
            {
                throw new ArgumentNullException("directory");
            }
            if (fileNameRoot == null)
            {
                throw new ArgumentNullException("fileNameRoot");
            }
            if (folders == null)
            {
                throw new ArgumentNullException("folders");
            }
            {
                var i = 0;
                foreach (var folder in folders)
                {
                    if (folder == null)
                    {
                        throw new ArgumentException(
                            string.Format(Strings.Argument_NullElement, i),
                            "folders");
                    }
                    ++i;
                }
            }

            _directory = directory;
            _fileNameRoot = fileNameRoot;
            _folders = folders.ToList();

            _nameHashToFolderMap
                = new Dictionary<long, ISqPackFolder>(_folders.Count);
            foreach (var folder in folders)
            {
                _nameHashToFolderMap.Add(folder.NameHash, folder);
            }
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="MutableSqPack"/>
        ///   by copying another <see cref="ISqPack"/> instance.
        /// </summary>
        /// <param name="other">
        ///   The instance of <see cref="ISqPack"/> to copy.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="other"/> is <ocde>null</ocde>.
        /// </throws>
        public MutableSqPack(ISqPack other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            _directory = new DirectoryInfo(other.Directory.FullName);
            _fileNameRoot = other.FileNameRoot;
            _folders = other.Folders.ToList();
            _nameHashToFolderMap = new Dictionary<long, ISqPackFolder>();
            foreach (var folder in _folders)
            {
                _nameHashToFolderMap.Add(folder.NameHash, folder);
            }
        }

        // Methods
        // ========================================================================

        /// <summary>
        ///   See <see cref="IMutableSqPack.Add(ISqPackFolder)"/>.
        /// </summary>
        public void Add(ISqPackFolder folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException("folder");
            }
            if (_nameHashToFolderMap.ContainsKey(folder.NameHash))
            {
                var otherFolder = _nameHashToFolderMap[folder.NameHash];
                if (folder == otherFolder)
                {
                    throw new ArgumentException(
                        Strings.Argument_DuplicatesExistingFolder,
                        "folder");
                }
                throw new ArgumentException(
                    Strings.Argument_DuplicatesExistingFolderNameHash,
                    "folder");
            }

            _folders.Add(folder);
            _nameHashToFolderMap.Add(folder.NameHash, folder);
        }

        /// <summary>
        ///   See <see
        ///   cref="IMutableSqPack.AddRange(IEnumerable{ISqPackFolder})"/>.
        /// </summary>
        public void AddRange(IEnumerable<ISqPackFolder> folders)
        {
            PrivateCheckAddRangeParams(folders);

            _folders.AddRange(folders);
            foreach (var folder in folders)
            {
                _nameHashToFolderMap.Add(folder.NameHash, folder);
            }
        }

        /// <summary>
        ///   See <see cref="ISqPack.AsMutable()"/>.
        /// </summary>
        public IMutableSqPack AsMutable()
        {
            return this;
        }

        /// <summary>
        ///   See <see cref="ISqPack.GetFolderByNameHash(long)"/>.
        /// </summary>
        public Option<ISqPackFolder> GetFolderByNameHash(long nameHash)
        {
            return SqPack.InternalGetFolderByNameHash(
                _nameHashToFolderMap,
                nameHash);
        }

        /// <summary>
        ///   See <see cref="ISqPack.GetFileByPathNameHash(long, long)"/>.
        /// </summary>
        public Option<ISqPackFile> GetFileByPathNameHash(
            long folderNameHash,
            long fileNameHash)
        {
            return SqPack.InternalGetFileByPathNameHash(
                _nameHashToFolderMap,
                folderNameHash,
                fileNameHash);
        }

        private void PrivateCheckAddRangeParams(IEnumerable<ISqPackFolder> folders)
        {
            if (folders == null)
            {
                throw new ArgumentNullException("folders");
            }

            // Construct an index-tagged list of the folders. We need the index to
            // report which element of "folders" is at fault.
            var folderList = new List<(int index, ISqPackFolder folder)>();
            {
                var i = 0;
                foreach (var folder in folders)
                {
                    folderList.Add((i, folder));
                    ++i;
                }
            }

            // Check for null elements.
            var nullFolders =
                from folderTuple in folderList
                where folderTuple.folder == null
                select folderTuple;
            if (nullFolders.Any())
            {
                var nullIndex = nullFolders.First().index;
                throw new ArgumentException(
                    string.Format(Strings.Argument_NullElement, nullIndex),
                    "folders");
            }

            // Check for internal duplicates. That is, an element that repeats
            // the NameHash of some previous element.
            var internalDuplicates =
                from folderTuple in folderList
                group folderTuple by folderTuple.folder.NameHash into foldersGroupedByNameHash
                // Select any groups such that there are any elements in the group
                // after skipping the first (i.e. Count() > 1)
                where foldersGroupedByNameHash.Skip(1).Any()
                from folderTuple in foldersGroupedByNameHash
                select folderTuple;
            if (internalDuplicates.Any())
            {
                var dupIdx1 = internalDuplicates.First().index;
                var dupIdx2 = internalDuplicates.Skip(1).First().index;
                throw new ArgumentException(
                    string.Format(
                        Strings.Argument_ElementDuplicatesPreviousNameHash,
                        dupIdx2,
                        dupIdx1),
                    "folders");
            }

            // Check for external duplicates. That is, an element that repeats
            // the NameHash of a folder already in the folder.
            var duplicateFolders =
                from folderTuple in folderList
                where _nameHashToFolderMap.ContainsKey(folderTuple.folder.NameHash)
                select folderTuple;
            if (duplicateFolders.Any())
            {
                // Distinguish between true duplicate folders and different folder
                // objects with the same namehash for more useful error
                // reporting.
                var (duplicateIdx, folder) = duplicateFolders.First();
                if (_nameHashToFolderMap[folder.NameHash] == folder)
                {
                    throw new ArgumentException(
                        string.Format(
                            Strings.Argument_ElementDuplicatesExistingFolder,
                            duplicateIdx),
                        "folders");
                }
                throw new ArgumentException(
                    string.Format(
                        Strings.Argument_ElementDuplicatesExistingFolderNameHash,
                        duplicateIdx),
                    "folders");
            }
        }

        /// <summary>
        ///   See <see cref="IMutableSqPack.Remove(ISqPackFolder)"/>.
        /// </summary>
        public void Remove(ISqPackFolder folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException("folder");
            }
            if (!_nameHashToFolderMap.ContainsKey(folder.NameHash))
            {
                throw new ArgumentException(
                    Strings.Argument_NonExistentFolder,
                    "folder");
            }

            _folders.Remove(folder);
            _nameHashToFolderMap.Remove(folder.NameHash);
        }

        /// <summary>
        ///   See <see cref="ISqPack.ToMutable()"/>.
        /// </summary>
        public IMutableSqPack ToMutable()
        {
            return new MutableSqPack(this);
        }

        /// <summary>
        ///   See <see cref="IMutableSqPack.ToImmutable()"/>.
        /// </summary>
        public ISqPack ToImmutable()
        {
            return new SqPack(this);
        }
    }
}
