namespace IronWorks.Structures
{
    using System;

    /// <summary>
    ///   Implementation of <see cref="ISqPackFile"/>
    /// </summary>
    public class SqPackFile : ISqPackFile
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFile.DataOffset"/>.
        /// </summary>
        public long DataOffset
        {
            get { return _dataOffset; }
        }

        /// <summary>
        ///   See <see cref="ISqPackFile.DataVolumeId"/>.
        /// </summary>
        public byte DataVolumeId
        {
            get { return _dataVolumeId; }
        }

        /// <summary>
        ///   See <see cref="ISqPackFile.IsMutable"/>.
        /// </summary>
        public bool IsMutable {
            get { return false; }
        }

        /// <summary>
        ///   See <see cref="ISqPackFile.NameHash"/>.
        /// </summary>
        public long NameHash
        {
            get { return _nameHash; }
        }

        // Fields
        // ========================================================================

        private readonly long _dataOffset;

        private readonly byte _dataVolumeId;

        private readonly long _nameHash;

        // Ctors
        // ========================================================================

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPackFile"/> class
        ///   with the specified file name hash.
        /// </summary>
        /// <param name="nameHash">
        ///   The CRC32-SE hash of the file name.
        /// </param>
        /// <param name="dataOffset">
        ///   The offset, in bytes, to the file's data stream, relative to the
        ///   beginning of its data volume.
        /// </param>
        /// <param name="dataVolumeId">
        ///   A number indicating which data volume (.dat file ) contains the
        ///   file's data.
        /// </param>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="dataOffset"/> is negative.
        /// </throws>
        public SqPackFile(
            long nameHash,
            byte dataVolumeId,
            long dataOffset)
        {
            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "nameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }
            if (dataOffset < 0)
            {
                throw new ArgumentOutOfRangeException(
                    "dataOffset",
                    Strings.ArgumentOutOfRange_NegativeFileOffset);
            }
            _nameHash = nameHash;
            _dataVolumeId = dataVolumeId;
            _dataOffset = dataOffset;
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="SqPackFile"/>
        ///   by copying another <see cref="ISqPackFile"/> instance.
        /// </summary>
        /// <param name="other">
        ///   The instance of <see cref="ISqPackFile"/> to copy.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="other"/> is <ocde>null</ocde>.
        /// </throws>
        public SqPackFile(ISqPackFile other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            _nameHash = other.NameHash;
            _dataVolumeId = other.DataVolumeId;
            _dataOffset = other.DataOffset;
        }

        // Methods
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFile.AsMutable()"/>.
        /// </summary>
        public IMutableSqPackFile AsMutable()
        {
            throw new NotSupportedException(
                Strings.NotSupported_Immutable);
        }

        /// <summary>
        ///   See <see cref="ISqPackFile.ToMutable()"/>.
        /// </summary>
        public IMutableSqPackFile ToMutable()
        {
            return new MutableSqPackFile(this);
        }
    }
}
