namespace IronWorks.Structures
{
    /// <summary>
    ///   Enum defining the types of SqPack data streams.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     It is probably unwise to assume that the numeric value of the
    ///     enum members correlate to the underlying field value in the
    ///     SqPack .dat file.
    ///   </para>
    ///   <para>
    ///     That being said, they do in fact correlate, and I see no reason to
    ///     ever change that, since it makes the parser logic very simple.
    ///   </para>
    /// </remarks>
    public enum SqPackDataStreamType : byte
    {
        /// <summary>
        ///   Indicates that the data stream encodes an arbitrary binary file.
        /// </summary>
        Binary = 2,

        /// <summary>
        ///   Indicates that the data stream encodes a model file.
        /// </summary>
        Model = 3,

        /// <summary>
        ///   Indicates that the data stream encodes a texture file.
        /// </summary>
        Texture = 4
    }
}
