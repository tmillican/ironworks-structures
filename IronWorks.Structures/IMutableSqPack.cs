namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///   Interface representing a mutable SqPack archive.
    /// </summary>
    public interface IMutableSqPack : ISqPack
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   Gets or sets a <see cref="DirectoryInfo"/> object identifying the
        ///   directory that contains the SqPack archive's files (.index and
        ///   .dat files).
        /// </summary>
        /// <value>
        ///   A <see cref="DirectoryInfo"/> object identifying the directory
        ///   that contains the SqPack archive's files (.index and .dat files).
        /// </value>
        /// <throws cref="ArgumentNullException">
        ///   The operation is set and <paramref name="value"/> is
        ///   <code>null</code>.
        /// </throws>
        new DirectoryInfo Directory { get; set; }

        /// <summary>
        ///   Gets or sets a string containing the file name root of the SqPack
        ///   archive.
        /// </summary>
        /// <value>
        ///   A string containing the file name root of the SqPack archive.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     For example, an archive with index file name of
        ///     "/users/john.doe/my_archive.index" would have a file name root
        ///     of "my_archive".
        ///   </para>
        /// </remarks>
        /// <throws cref="ArgumentNullException">
        ///   The operation is set and <paramref name="value"/> is
        ///   <code>null</code>.
        /// </throws>
        new string FileNameRoot { get; set; }

        // Methods
        // ========================================================================

        /// <summary>
        ///   Adds a folder to the <see cref="IMutableSqPack"/>.
        /// </summary>
        /// <param name="folder">
        ///   An <see cref="ISqPackFolder"/> instance representing the folder.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   <paramref name="folder"/> duplicates the <see
        ///   cref="ISqPackFolder.NameHash"/> property of any existing folder in
        ///   the <see cref="IMutableSqPack"/>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   If <paramref name="folder"/> is <code>null</code>.
        /// </throws>
        void Add(ISqPackFolder folder);

        /// <summary>
        ///   Adds a range of folders to the <see cref="IMutableSqPack"/>.
        /// </summary>
        /// <param name="folders">
        ///   An enumerable of <see cref="ISqPackFolder"/> instances representing
        ///   the folders.
        /// </param>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="folders"/> is <code>null</code>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="folders"/> duplicates the <see
        ///   cref="ISqPackFolder.NameHash"/> property of any existing folder in the
        ///   <see cref="IMutableSqPack"/>.
        /// </throws>
        /// <throws cref="ArgumentException">
        ///   Any element of <paramref name="folders"/> duplicates the <see
        ///   cref="ISqPackFolder.NameHash"/> property of any other element in
        ///   <paramref name="folders"/>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   If <paramref name="folders"/> is <code>null</code>.
        /// </throws>
        void AddRange(IEnumerable<ISqPackFolder> folders);

        /// <summary>
        ///   Removes the specified <see cref="ISqPackFolder"/> instance from
        ///   the <see cref="IMutableSqPack"/>.
        /// </summary>
        /// <throws cref="ArgumentException">
        ///   The <see cref="IMutableSqPack"/> does not contain <paramref
        ///   name="folder"/>.
        /// </throws>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="folder"/> is <code>null</code>.
        /// </throws>
        void Remove(ISqPackFolder folder);

        /// <summary>
        ///   Produces an immutable copy of this <see cref="IMutableSqPack"/>.
        /// </summary>
        /// <returns>
        ///   An immutable copy of this <see cref="IMutableSqPack"/>.
        /// </returns>
        ISqPack ToImmutable();
    }
}
