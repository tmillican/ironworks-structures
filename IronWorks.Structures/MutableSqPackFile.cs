namespace IronWorks.Structures
{
    using System;

    /// <summary>
    ///   Implementation of <see cref="IMutableSqPackFile"/>.
    /// </summary>
    public class MutableSqPackFile : IMutableSqPackFile
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFile.IsMutable"/>.
        /// </summary>
        public bool IsMutable { get { return true; } }

        /// <summary>
        ///   See <see cref="IMutableSqPackFile.DataOffset"/>.
        /// </summary>
        public long DataOffset
        {
            get { return _dataOffset; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(
                        "value",
                        Strings.ArgumentOutOfRange_NegativeFileOffset);
                }
            }
        }

        /// <summary>
        ///   See <see cref="IMutableSqPackFile.DataVolumeId"/>.
        /// </summary>
        public byte DataVolumeId
        {
            get { return _dataVolumeId; }
            set { _dataVolumeId = value; }
        }

        /// <summary>
        ///   See <see cref="IMutableSqPackFile.NameHash"/>.
        /// </summary>
        public long NameHash
        {
            get { return _nameHash; }
            set
            {
                if (value < 0 || value > uint.MaxValue)
                {
                    throw new ArgumentOutOfRangeException(
                        "value",
                        string.Format(
                            Strings.ArgumentOutOfRange_Range,
                            0,
                            uint.MaxValue));
                }
                _nameHash = value;
            }
        }

        // Fields
        // ========================================================================

        private long _nameHash;

        private long _dataOffset;

        private byte _dataVolumeId;

        // Ctors
        // ========================================================================

        /// <summary>
        ///   Initializes a new instance of the <see cref="MutableSqPackFile"/>
        ///   class with the specified file name hash.
        /// </summary>
        /// <param name="nameHash">
        ///   The CRC32-SE hash of the file name.
        /// </param>
        /// <param name="dataOffset">
        ///   The offset, in bytes, to the file's data stream, relative to the
        ///   beginning of its data volume.
        /// </param>
        /// <param name="dataVolumeId">
        ///   A number indicating which data volume (.dat file ) contains the
        ///   file's data.
        /// </param>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="dataOffset"/> is negative.
        /// </throws>
        public MutableSqPackFile(
            long nameHash,
            byte dataVolumeId,
            long dataOffset)
        {
            if (nameHash < 0 || nameHash > uint.MaxValue)
            {
                throw new ArgumentOutOfRangeException(
                    "nameHash",
                    string.Format(
                        Strings.ArgumentOutOfRange_Range,
                        0,
                        uint.MaxValue));
            }
            if (dataOffset < 0)
            {
                throw new ArgumentOutOfRangeException(
                    "dataOffset",
                    Strings.ArgumentOutOfRange_NegativeFileOffset);
            }
            _nameHash = nameHash;
            _dataVolumeId = dataVolumeId;
            _dataOffset = dataOffset;
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="MutableSqPackFile"/>
        ///   by copying another <see cref="ISqPackFile"/> instance.
        /// </summary>
        /// <param name="other">
        ///   The instance of <see cref="ISqPackFile"/> to copy.
        /// </param>
        /// <throws cref="ArgumentNullException">
        ///   <paramref name="other"/> is <ocde>null</ocde>.
        /// </throws>
        public MutableSqPackFile(ISqPackFile other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            _nameHash = other.NameHash;
            _dataVolumeId = other.DataVolumeId;
            _dataOffset = other.DataOffset;
        }

        // Methods
        // ========================================================================

        /// <summary>
        ///   See <see cref="ISqPackFile.AsMutable()"/>.
        /// </summary>
        public IMutableSqPackFile AsMutable()
        {
            return this;
        }

        /// <summary>
        ///   See <see cref="ISqPackFile.ToMutable()"/>.
        /// </summary>
        public IMutableSqPackFile ToMutable()
        {
            return new MutableSqPackFile(this);
        }

        /// <summary>
        ///   See <see cref="IMutableSqPackFile.ToImmutable()"/>.
        /// </summary>
        public ISqPackFile ToImmutable()
        {
            return new SqPackFile(this);
        }
    }
}
