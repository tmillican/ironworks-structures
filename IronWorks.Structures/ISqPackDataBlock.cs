namespace IronWorks.Structures
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///   Interface representing a data block in an SqPack file data stream.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Each block is either a block of raw uncompressed data, or a
    ///     compressed DEFLATE (RFC-1951) stream.
    ///   </para>
    /// </remarks>
    public interface ISqPackDataBlock
    {
        /// <summary>
        ///   Gets a value indicating whether the data block is compressed.
        /// </summary>
        /// <value>
        ///   If the data block is compressed, <code>true</code>. Otherwise,
        ///   <code>false</code>.
        /// </value>
        bool IsCompressed { get; }

        /// <summary>
        ///   Gets a value indicating whether the data block is mutable.
        /// </summary>
        /// <value>
        ///   If the data block is mutable, <code>true</code>. Otherwise,
        ///   <code>false</code>.
        /// </value>
        bool IsMutable { get; }

        /// <summary>
        ///   Gets a read-only list of bytes containing the block's data.
        /// </summary>
        /// <value>
        ///   A read-only list of bytes containing the block's data.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     If <see cref="IsCompressed"/> is <code>true</code>, this is an
        ///     RFC-1951 stream. Otherwise, it is raw uncompressed file data.
        ///   </para>
        /// </remarks>
        IReadOnlyList<byte> Data { get; }

        /// <summary>
        ///   Gets the uncompressed size of the data block.
        /// </summary>
        /// <value>
        ///   The uncompressed size of the data block, in bytes.
        /// </value>
        int UncompressedSize { get; }

        /// <summary>
        ///   Unpacks the data block.
        /// </summary>
        /// <returns>
        ///   The unpacked block data. If <see cref="IsCompressed"/> is
        ///   <code>false</code>, returns a mutable copy of <see cref="Data"/>.
        /// </returns>
        byte[] Unpack();

        /// <summary>
        ///   Unpacks the data stream, writing the result to the provided
        ///   output <see cref="Stream"/>.
        /// </summary>
        void Unpack(Stream outStream);
    }
}
