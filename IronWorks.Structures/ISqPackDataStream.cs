namespace IronWorks.Structures
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///   Interface representing a file data stream in an SqPack data volume
    ///   (.dat file).
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Internally, there are three kinds of data streams: binary, model,
    ///     and texture. This interface abstracts the differences.
    ///   </para>
    ///   <para>
    ///     In general, each data stream consists of a sequence of "blocks",
    ///     each of which is either uncompressed data or a compressed DEFLATE
    ///     (RFC-1951) stream. See also: <see cref="ISqPackDataBlock"/>.
    ///   </para>
    ///   <para>
    ///     Model and texture streams organize their blocks into "frames", but
    ///     this is generally irrelevant beyond parsing, so the frames are
    ///     logically flattened into a single sequence of blocks.  Additionally,
    ///     a texture stream contains an uncompressed file header.
    ///   </para>
    /// </remarks>
    public interface ISqPackDataStream
    {
        /// <summary>
        ///   Gets a value indicating whether the data stream is mutable.
        /// </summary>
        /// <value>
        ///   If the data stream is mutable, <code>true</code>. Otherwise,
        ///   <code>false</code>.
        /// </value>
        bool IsMutable { get; }

        /// <summary>
        ///   Gets a read-only list of blocks comprising the data stream (file
        ///   data).
        /// </summary>
        /// <value>
        ///   Gets a read-only list of <see cref="ISqPackDataBlock"/> instances
        ///   comprising the data stream (file data).
        /// </value>
        IReadOnlyList<ISqPackDataBlock> Blocks { get; }

        /// <summary>
        ///   Gets a read-only list of bytes comprising the original
        ///   uncompressed file header.
        /// </summary>
        /// <value>
        ///   A list of bytes comprising the original uncompressed file header.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     If <see cref="ContentType"/> is anything other than <see
        ///     cref="SqPackDataStreamType.Texture"/>, this will be an empty
        ///     list.
        ///   </para>
        /// </remarks>
        IReadOnlyList<byte> OriginalHeader { get; }

        /// <summary>
        ///   Gets an enum value indicating the content type of the data stream.
        /// </summary>
        /// <value>
        ///   A <see cref="SqPackDataStreamType"/> enum value indicating the
        ///   content type of the data stream.
        /// </value>
        SqPackDataStreamType ContentType { get; }

        /// <summary>
        ///   Gets the uncompressed size of the data block.
        /// </summary>
        /// <value>
        ///   The uncompressed size of the data block, in bytes.
        /// </value>
        /// <remarks>
        ///   <para>
        ///     This includes the size of <see cref="OriginalHeader"/> for
        ///     texture streams (when <code>ContentType ==
        ///     SqPackDataStreamType.Texture</code>).
        ///   </para>
        /// </remarks>
        int UncompressedSize { get; }

        /// <summary>
        ///   Gets the file extension associated with this stream's <see
        ///   cref="ContentType"/>.
        /// </summary>
        /// <value>
        ///   The file extension associated with this stream's <see
        ///   cref="ContentType"/>.
        /// </value>
        string FileExtension { get; }

        /// <summary>
        ///   Unpacks the data stream to an array.
        /// </summary>
        /// <returns>
        ///   An array of bytes containing the unpacked data stream.
        /// </returns>
        byte[] Unpack();

        /// <summary>
        ///   Unpacks the data stream, writing the result to the provided
        ///   output <see cref="Stream"/>.
        /// </summary>
        void Unpack(Stream outStream);
    }
}
