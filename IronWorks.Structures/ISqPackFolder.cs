namespace IronWorks.Structures
{
    using System;
    using System.Collections.Generic;

    using IronWorks.Monads;

    /// <summary>
    ///   Interface representing a folder stored in an SqPack archive.
    /// </summary>
    public interface ISqPackFolder
    {
        // Properties
        // ========================================================================

        /// <summary>
        ///   Gets a read-only list of the files contained in the <see
        ///   cref="ISqPackFolder"/>.
        /// </summary>
        /// <value>
        ///   A read-only list of <see cref="ISqPackFile"/> instances representing the
        ///   files contained in the <see cref="ISqPackFolder"/>.
        /// </value>
        IReadOnlyList<ISqPackFile> Files { get; }

        /// <summary>
        ///   Gets a value indicating whether this <see cref="ISqPackFolder"/>
        ///   instance is mutable.
        /// </summary>
        /// <value>
        ///   If this instance is mutable, <code>true</code>. Otherwise
        ///   <code>false</code>.
        /// </value>
        bool IsMutable { get; }

        /// <summary>
        ///   Gets the CRC-32SE hash of the folder name.
        /// </summary>
        /// <value>
        ///   The CRC-32SE hash of the folder name.
        /// </value>
        long NameHash { get; }

        // Methods
        // ========================================================================

        /// <summary>
        ///   Casts this <see cref="ISqPackFolder"/> to an <see
        ///   cref="IMutableSqPackFolder"/> instance.
        /// </summary>
        /// <throws cref="NotSupportedException">
        ///   <see cref="IsMutable"/> is <code>false</code>.
        /// </throws>
        /// <returns>
        ///   A cast of this <see cref="ISqPackFolder"/> to <see
        ///   cref="IMutableSqPackFolder"/>.
        /// </returns>
        IMutableSqPackFolder AsMutable();

        /// <summary>
        ///   Gets the <see cref="ISqPackFile"/> instance identified by
        ///   <paramref name="nameHash"/> in the <see cref="ISqPackFolder"/>.
        /// </summary>
        /// <value>
        ///   A <code>Option&lt;ISqPackFile&gt;</code> object containing the
        ///   result of the get.
        /// </value>
        /// <throws cref="ArgumentOutOfRangeException">
        ///   <paramref name="nameHash"/> is less than <code>0</code> or greater
        ///   than <code>UInt32.MaxValue</code>.
        /// </throws>
        Option<ISqPackFile> GetFileByNameHash(long nameHash);

        /// <summary>
        ///   Produces a mutable copy of this <see cref="ISqPackFolder"/>.
        /// </summary>
        /// <returns>
        ///   A mutable copy of this <see cref="ISqPackFolder"/>.
        /// </returns>
        /// <remarks>
        ///   <para>
        ///     If <see cref="IsMutable"/> is <code>true</code>, you may wish to
        ///     avoid the copy by casting the instance with <see
        ///     cref="AsMutable()"/>.
        ///   </para>
        /// </remarks>
        IMutableSqPackFolder ToMutable();
    }
}
