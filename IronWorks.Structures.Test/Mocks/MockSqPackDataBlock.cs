namespace IronWorks.Structures.Test
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using IronWorks.Structures;

    /// <summary>
    ///   Mock implementtion of <see cref="ISqPackDataBlock"/>.
    /// </summary>
    public class MockSqPackDataBlock : ISqPackDataBlock
    {
        static readonly byte[] _payLoad = new byte[] {
            0xDE, 0xAD, 0xBE, 0xEF
        };

        public bool IsCompressed { get { return false; } }

        public bool IsMutable { get { return false; } }

        public IReadOnlyList<byte> Data { get { return _payLoad; } }

        public int UncompressedSize { get { return 4; } }

        public byte[] Unpack() { return _payLoad.ToArray(); }

        public void Unpack(Stream outStream)
        {
            outStream.Write(_payLoad, 0, _payLoad.Length);
        }
    }
}
