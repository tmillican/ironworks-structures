namespace IronWorks.Structures.Test
{
    using System;

    /// <summary>
    ///   Mock implementation of <see cref="IMutableSqPackFile"/> (and <see
    ///   cref="ISqPackFile"/> by derivation).
    /// </summary>
    public class MockSqPackFile : IMutableSqPackFile
    {
        // Properties
        // ========================================================================

        // We only pretend to be mutable...
        public long DataOffset
        {
            get { return _dataOffset; }
            set { }
        }

        public byte DataVolumeId
        {
            get { return _dataVolumeId; }
            set { }
        }

        public bool IsMutable { get { return true; } }

        public long NameHash
        {
            get { return _nameHash; }
            set { }
        }

        // Fields
        // ========================================================================

        private readonly long _dataOffset;

        private readonly byte _dataVolumeId;

        private readonly long _nameHash;

        // Ctors
        // ========================================================================

        public MockSqPackFile(
            long nameHash = 0,
            byte dataVolumeId = 0,
            long dataOffset = 0)
        {
            _nameHash = nameHash;
            _dataVolumeId = dataVolumeId;
            _dataOffset = dataOffset;
        }

        // Methods
        // ========================================================================

        public IMutableSqPackFile AsMutable() { return this; }

        public IMutableSqPackFile ToMutable()
        {
            return new MockSqPackFile(_nameHash, _dataVolumeId, _dataOffset);
        }

        public ISqPackFile ToImmutable()
        {
            return new MockSqPackFile(_nameHash, _dataVolumeId, _dataOffset);
        }
    }
}
