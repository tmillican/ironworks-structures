namespace IronWorks.Structures.Test
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;

    using IronWorks.Monads;

    /// <summary>
    ///   Mock implementation of <see cref="IMutableSqPackFolder"/> (and <see
    ///   cref="ISqPackFolder"/> by derivation).
    /// </summary>
    public class MockSqPackFolder : IMutableSqPackFolder
    {
        // Properties
        // ========================================================================

        public IReadOnlyList<ISqPackFile> Files
        {
            get { return _files; }
            set { }
        }

        public bool IsMutable { get { return true; } }

        public long NameHash
        {
            get { return _nameHash; }
            set { }
        }

        // Fields
        // ========================================================================

        private readonly long _nameHash;

        private static readonly ImmutableList<ISqPackFile> _files
            = new ISqPackFile[0].ToImmutableList();

        // Ctors
        // ========================================================================

        public MockSqPackFolder(long nameHash = 0)
        {
            _nameHash = nameHash;
        }

        public MockSqPackFolder(ISqPackFolder other)
        {
            _nameHash = other.NameHash;
        }

        // Methods
        // ========================================================================

        public void Add(ISqPackFile file) { }

        public IMutableSqPackFolder AsMutable() { return this; }

        public void AddRange(IEnumerable<ISqPackFile> files) { }

        public Option<ISqPackFile> GetFileByNameHash(long nameHash)
        {
            return Option<ISqPackFile>.None();
        }

        public void Remove(ISqPackFile file) { }

        public IMutableSqPackFolder ToMutable()
        {
            return new MockSqPackFolder(this);
        }

        public ISqPackFolder ToImmutable()
        {
            return new MockSqPackFolder(this);
        }
    }
}
