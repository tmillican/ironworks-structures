namespace IronWorks.Structures.Test
{
    using System;
    using System.Collections.Generic;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;
    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="MutableSqPackFolder"/>.
    /// </summary>
    public static class MutableSqPackFolderTests
    {
        private static readonly
            List<ISqPackFile> _emptyFiles = new List<ISqPackFile>();

        // Tests
        // ========================================================================

        // Ctor
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            Action ctor = () => new MutableSqPackFolder(nameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Ctor_ShouldThrowArgumentNullException_WithNullFiles()
        {
            Action ctor = () => new MutableSqPackFolder(0, null);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'files' cannot be null")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentException_WithNullElementInFiles()
        {
            var files = new ISqPackFile[3] {
                new MockSqPackFile(0),
                null,
                new MockSqPackFile(2),
            };
            Action ctor = () => new MutableSqPackFolder(0, files);
            var expectedMessage = string.Format(
                Strings.Argument_NullElement, 1);

            ctor.Should().Throw<ArgumentException>(
                "because 'files' cannot contain null references")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldYieldEmptyFiles_WhenFilesParameterIsOmitted()
        {
            new MutableSqPackFolder(0).Files
                .Should().BeEmpty(
                    "because the 'files' parameter was omitted");
        }

        // IsMutable property
        // ------------------------------------------------------------------------

        [Fact]
        public static void IsMutable_ShouldBeTrue()
        {
            new MutableSqPackFolder(0).IsMutable
                .Should().BeTrue("because MutableSqPackFolder is mutable");
        }

        // NameHash property
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        SetNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long value)
        {
            Action setNameHash = () =>
                new MutableSqPackFolder(0).NameHash = value;
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            setNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "value",
                    "because we fully describe our exceptions");
        }

        // void Add(ISqPackFile file)
        // ------------------------------------------------------------------------

        [Fact]
        public static void Add_ShouldThrowArgumentNullException_WithNullFile()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            Action addMethod = () => folder.Add(null);

            addMethod.Should().Throw<ArgumentNullException>(
                "because 'file' cannot be null")
                .And.ParamName.Should().Be(
                    "file",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Add_ShouldThrowArgumentException_WithDuplicateFile()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var file = new MockSqPackFile(0);
            folder.Add(file);
            Action addMethod = () => folder.Add(file);

            addMethod.Should().Throw<ArgumentException>(
                "because 'file' duplicates an existing file")
                .WithMessage(
                    $"{Strings.Argument_DuplicatesExistingFile}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "file",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Add_ShouldThrowArgumentException_WithDuplicateNameHash()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var fileA = new MockSqPackFile(0);
            var fileB = new MockSqPackFile(0);
            folder.Add(fileA);
            Action addMethod = () => folder.Add(fileB);

            addMethod.Should().Throw<ArgumentException>(
                "because 'file' duplicates an existing file")
                .WithMessage(
                    $"{Strings.Argument_DuplicatesExistingFileNameHash}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "file",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Add_ShouldUpdateFolder_WhenSuccessful()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var file = new MockSqPackFile();
            folder.Add(file);

            folder.Files
                .Should().Contain(
                    file,
                    "because the file was added to the 'Files' property");
            folder.GetFileByNameHash(file.NameHash).Match(
                some: sv => sv,
                none: nv => null)
                .Should().Be(file, "because the name hash map was updated");
        }

        // void AddRange(IEnumerable<ISqPackFile> files)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentNullException_WithNullFiles()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            Action addRange = () => folder.AddRange(null);

            addRange.Should().Throw<ArgumentNullException>(
                "because 'files' cannot be null")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentNullException_WithNullFileElement()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var files = new ISqPackFile[] {
                new MockSqPackFile(0),
                null,
                new MockSqPackFile(2),
            };
            Action addRange = () => folder.AddRange(files);
            var exepectedMessage =
                string.Format(Strings.Argument_NullElement, 1);

            addRange.Should().Throw<ArgumentException>(
                "because 'files' cannot contain null references")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentException_WhenFilesDuplicatesAnyExistingFile()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var existingFiles = new ISqPackFile[] {
                new MockSqPackFile(0),
                new MockSqPackFile(1),
                new MockSqPackFile(2),
            };
            var newFiles = new ISqPackFile[] {
                new MockSqPackFile(3),
                new MockSqPackFile(4),
                existingFiles[1],
            };
            folder.AddRange(existingFiles);

            Action addRange = () => folder.AddRange(newFiles);
            var expectedMessage = string.Format(
                Strings.Argument_ElementDuplicatesExistingFile,
                2);

            addRange.Should().Throw<ArgumentException>(
                "because 'files' contains a duplicate of an existing file")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentException_WhenFilesDuplicatesAnyExistingNameHash()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var existingFiles = new ISqPackFile[] {
                new MockSqPackFile(0),
                new MockSqPackFile(1),
                new MockSqPackFile(2),
            };
            var newFiles = new ISqPackFile[] {
                new MockSqPackFile(3),
                new MockSqPackFile(4),
                new MockSqPackFile(1),
            };
            folder.AddRange(existingFiles);

            Action addRange = () => folder.AddRange(newFiles);
            var expectedMessage = string.Format(
                Strings.Argument_ElementDuplicatesExistingFileNameHash,
                2);

            addRange.Should().Throw<ArgumentException>(
                "because 'files' contains a duplicate of an existing file")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentException_WhenFilesInternallyRepeatsAnyNameHash()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var newFiles = new ISqPackFile[] {
                new MockSqPackFile(99),
                new MockSqPackFile(4),
                new MockSqPackFile(99),
            };

            Action addRange = () => folder.AddRange(newFiles);
            var expectedMessage = string.Format(
                Strings.Argument_ElementDuplicatesPreviousNameHash,
                2,
                0);

            addRange.Should().Throw<ArgumentException>(
                "because 'files' contains multiple elements with the same 'NameHash' value")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void AddRange_ShouldUpdateFolder_WhenSuccessful()
        {
            IMutableSqPackFolder folder =
                new MutableSqPackFolder(0, _emptyFiles);
            var files = new ISqPackFile[3];
            for (var i = 0; i < files.Length; ++i)
            {
                files[i] = new MockSqPackFile(i);
            }
            folder.AddRange(files);

            foreach (var file in files)
            {
                folder.Files
                    .Should().Contain(
                        file,
                        "because the file was added to the 'Files' property");
                folder.GetFileByNameHash(file.NameHash).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(file, "because the name hash map was updated");
            }
        }

        // IMutableSqPackFolder AsMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void AsMutable_ShouldReturnThis()
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);

            object.ReferenceEquals(folder, folder.AsMutable())
                .Should().BeTrue(
                    "Because AsMutable is a cast.");
        }

        // Option<ISqPackFile> GetFileByNameHash(long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFileByNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);
            Action getFileByNameHash = () =>
                folder.GetFileByNameHash(nameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFileByNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        GetFileByNameHash_ShouldReturnOptionNone_WithNonPresentHash()
        {
            new MutableSqPackFolder(0,
                       new MockSqPackFile[] {
                           new MockSqPackFile(0),
                           new MockSqPackFile(1),
                       })
                .GetFileByNameHash(5).IsNone
                .Should().BeTrue("because no such file exists");
        }

        [Fact]
        public static void
        GetFileByNameHash_ShouldReturnExpectedFile_WithPresentHash()
        {
            var files = new MockSqPackFile[] {
                new MockSqPackFile(0),
                new MockSqPackFile(1),
            };
            var folder = new MutableSqPackFolder(0, files);

            for (var i = 0; i < files.Length; ++i)
            {
                folder.GetFileByNameHash(i).Match(
                    some: value => value,
                    none: nv => null)
                    .Should().Be(
                        files[i],
                        "because the name hash map should be correct");
            }
        }

        // void Remove(ISqPackFile)
        // ------------------------------------------------------------------------

        [Fact]
        public static void Remove_ShouldThrowArgumentNullException_WithNullFile()
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);
            Action removeAction = () => folder.Remove(null);

            removeAction.Should().Throw<ArgumentNullException>(
                "because we cannot remove a null file")
                .And.ParamName.Should().Be(
                    "file",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Remove_ShouldThrowArgumentException_WithNonExistentFile()
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);
            Action removeAction = () => folder.Remove(new MockSqPackFile());

            removeAction.Should().Throw<ArgumentException>(
                "because we cannot remove a file that does not exist")
                .WithMessage(
                    $"{Strings.Argument_NonExistentFile}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "file",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Remove_ShouldUpdateFolder_WhenSuccessful()
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);
            var files = new ISqPackFile[] {
                new MockSqPackFile(10),
                new MockSqPackFile(20),
                new MockSqPackFile(30),
            };
            var remainingFiles = new ISqPackFile[] {
                files[0],
                files[2],
            };
            var removedFile = files[1];
            folder.AddRange(files);
            folder.Remove(removedFile);

            // This isn't a great "because" description, but BeEquivalentTo()
            // kills two tests with one stone:
            //    * Files should not contain the removed file.
            //    * Files should contain all of the remaining files, in their
            //      original order.
            folder.Files.Should().BeEquivalentTo(
                remainingFiles,
                "because Remove() should correctly update the 'Files' property");

            foreach (var remainingFile in remainingFiles)
            {
                folder.GetFileByNameHash(remainingFile.NameHash).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(
                        remainingFile,
                        "because Remove() should preserve remaining file name hash map entries");
            }

            folder.GetFileByNameHash(files[1].NameHash).IsNone
                .Should().BeTrue(
                    "because Remove() removes the target file's name hash map entry");
        }

        // IMutableSqPackFolder ToMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToMutable_ShouldReturnDistinctButEquivalentMutableSqPackFolder()
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);
            var mutableFolder = folder.ToMutable();

            object.ReferenceEquals(folder, mutableFolder)
                .Should().BeFalse("because ToMutable() is a copy, not a cast");

            mutableFolder.NameHash
                .Should().Be(
                    folder.NameHash,
                    "because ToMutable() produces an equivalent instance");

            mutableFolder.Files
                .Should().BeEquivalentTo(
                    folder.Files,
                    "because ToMutable() produces an equivalent instance");
        }

        // ISqPackFolder ToImmutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToImmutable_ShouldReturnDistinctButEquivalentSqPackFolder()
        {
            var folder = new MutableSqPackFolder(0, _emptyFiles);
            var immutableFolder = folder.ToImmutable();

            object.ReferenceEquals(folder, immutableFolder)
                .Should().BeFalse("because ToImmutable() is a copy, not a cast");

            immutableFolder.NameHash
                .Should().Be(
                    folder.NameHash,
                    "because ToImmutable() produces an equivalent instance");

            immutableFolder.Files
                .Should().BeEquivalentTo(
                    folder.Files,
                    "because ToImmutable() produces an equivalent instance");
        }
    }
}
