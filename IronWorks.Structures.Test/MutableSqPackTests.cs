namespace IronWorks.Structures.Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;
    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="MutableSqPack"/>.
    /// </summary>
    public static class MutableSqPackTests
    {
        private static List<ISqPackFolder> _emptyFolders = new List<ISqPackFolder>();

        private static DirectoryInfo _rootDirectory = new DirectoryInfo("/");

        // Tests
        // ========================================================================

        // MutableSqPack(DirectoryInfo directory, string fileNameRoot)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullDirectory()
        {
            Action ctor = () => new MutableSqPack(null, "", _emptyFolders);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'directory' cannot be null")
                .And.ParamName.Should().Be(
                    "directory",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullFileNameRoot()
        {
            Action ctor = () =>
                new MutableSqPack(_rootDirectory, null, _emptyFolders);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'fileNameRoot' cannot be null")
                .And.ParamName.Should().Be(
                    "fileNameRoot",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullFolders()
        {
            Action ctor = () => new MutableSqPack(_rootDirectory, "", null);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'folders' cannot be null")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentException_WithNullElementInFolders()
        {
            var folders = new ISqPackFolder[] {
                new MockSqPackFolder(0),
                null,
                new MockSqPackFolder(2),
            };
            Action ctor = () => new MutableSqPack(_rootDirectory, "", folders);
            var expectedMessage = string.Format(
                Strings.Argument_NullElement, 1);

            ctor.Should().Throw<ArgumentException>(
                "because 'folders' cannot contain null references")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldYieldEmptyFolders_WhenFoldersParameterIsOmitted()
        {
            new MutableSqPack(_rootDirectory, "").Folders
                .Should().BeEmpty(
                    "because the 'folders' parameter was omitted");
        }

        // MutableSqPack(ISqPack other)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        CopyCtor_ShouldThrowArgumentNullException_WithNullOther()
        {
            Action ctor = () => new MutableSqPack((ISqPack)null);

            ctor.Should().Throw<ArgumentNullException>(
                "because we cannot copy a null instance.")
                .And.ParamName.Should().Be(
                    "other",
                    "because we describe our exceptions");
        }

        // Directory property
        // ------------------------------------------------------------------------

        [Fact]
        public static void SetDirectory_ShouldThrowArgumentNullException_WithNullValue()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action setDirectory = () => pack.Directory = null;

            setDirectory.Should().Throw<ArgumentNullException>(
                "because a non-null value is required")
                .And.ParamName.Should().Be(
                    "value",
                    "because we fully describe our exceptions");
        }

        // FileNameRoot property
        // ------------------------------------------------------------------------

        [Fact]
        public static void SetFileNameRoot_ShouldThrowArgumentNullException_WithNullValue()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action setFileNameRoot = () => pack.FileNameRoot = null;

            setFileNameRoot.Should().Throw<ArgumentNullException>(
                "because a non-null value is required")
                .And.ParamName.Should().Be(
                    "value",
                    "because we fully describe our exceptions");
        }

        // IsMutable property
        // ------------------------------------------------------------------------

        [Fact]
        public static void IsMutable_ShouldBeTrue()
        {
            new MutableSqPack(_rootDirectory, "", _emptyFolders)
                .IsMutable
                .Should().BeTrue();
        }

        // Option<ISqPackFolder> GetFolderByNameHash(long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFolderByNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action getFolderByNameHash = () =>
                pack.GetFolderByNameHash(nameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFolderByNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        GetFolderByNameHash_ShouldReturnOptionNone_WithNonPresentHash()
        {
            new MutableSqPack(_rootDirectory,
                       "",
                       new MockSqPackFolder[] {
                           new MockSqPackFolder(0),
                           new MockSqPackFolder(1),
                       })
                .GetFolderByNameHash(5).IsNone
                .Should().BeTrue("because no such folder exists");
        }

        [Fact]
        public static void
        GetFolderByNameHash_ShouldReturnExpectedFolder_WithPresentHash()
        {
            var folders = new MockSqPackFolder[] {
                new MockSqPackFolder(0),
                new MockSqPackFolder(1),
            };
            var pack = new MutableSqPack(_rootDirectory, "", folders);

            for (var i = 0; i < folders.Length; ++i)
            {
                pack.GetFolderByNameHash(i).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(
                        folders[i],
                        "because the name hash map should be correct");
            }
        }

        // void Add(ISqPackFolder folder)
        // ------------------------------------------------------------------------

        [Fact]
        public static void Add_ShouldThrowArgumentNullException_WithNullFolder()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action addMethod = () => pack.Add((ISqPackFolder)null);

            addMethod.Should().Throw<ArgumentNullException>(
                "because 'folder' cannot be null")
                .And.ParamName.Should().Be(
                    "folder",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Add_ShouldThrowArgumentException_WithDuplicateFolder()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var folder = new MockSqPackFolder(0);
            pack.Add(folder);
            Action addMethod = () => pack.Add(folder);

            addMethod.Should().Throw<ArgumentException>(
                "because 'folder' duplicates an existing folder")
                .WithMessage(
                    $"{Strings.Argument_DuplicatesExistingFolder}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folder",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Add_ShouldThrowArgumentException_WithDuplicateNameHash()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var folderA = new MockSqPackFolder(0);
            var folderB = new MockSqPackFolder(0);
            pack.Add(folderA);
            Action addMethod = () => pack.Add(folderB);

            addMethod.Should().Throw<ArgumentException>(
                "because 'folder' duplicates an existing folder")
                .WithMessage(
                    $"{Strings.Argument_DuplicatesExistingFolderNameHash}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folder",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Add_ShouldUpdatePack_WhenSuccessful()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var folder = new MockSqPackFolder();
            pack.Add(folder);

            pack.Folders
                .Should().Contain(
                    folder,
                    "because the folder was added to the 'Folders' property");
            pack.GetFolderByNameHash(folder.NameHash).Match(
                some: sv => sv,
                none: nv => null)
                .Should().Be(folder, "because the name hash map was updated");
        }

        // void AddRange(IEnumerable<ISqPackFolder> folders)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentNullException_WithNullFolders()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action addRange = () => pack.AddRange(null);

            addRange.Should().Throw<ArgumentNullException>(
                "because 'folders' cannot be null")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentNullException_WithNullFolderElement()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var folders = new ISqPackFolder[] {
                new MockSqPackFolder(0),
                null,
                new MockSqPackFolder(2),
            };
            Action addRange = () => pack.AddRange(folders);
            var exepectedMessage =
                string.Format(Strings.Argument_NullElement, 1);

            addRange.Should().Throw<ArgumentException>(
                "because 'folders' cannot contain null references")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentException_WhenFoldersDuplicatesAnyExistingFolder()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var existingFolders = new ISqPackFolder[] {
                new MockSqPackFolder(0),
                new MockSqPackFolder(1),
                new MockSqPackFolder(2),
            };
            var newFolders = new ISqPackFolder[] {
                new MockSqPackFolder(3),
                new MockSqPackFolder(4),
                existingFolders[1],
            };
            pack.AddRange(existingFolders);

            Action addRange = () => pack.AddRange(newFolders);
            var expectedMessage = string.Format(
                Strings.Argument_ElementDuplicatesExistingFolder,
                2);

            addRange.Should().Throw<ArgumentException>(
                "because 'folders' contains a duplicate of an existing folder")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentException_WhenFoldersDuplicatesAnyExistingNameHash()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var existingFolders = new ISqPackFolder[] {
                new MockSqPackFolder(0),
                new MockSqPackFolder(1),
                new MockSqPackFolder(2),
            };
            var newFolders = new ISqPackFolder[] {
                new MockSqPackFolder(3),
                new MockSqPackFolder(4),
                new MockSqPackFolder(1),
            };
            pack.AddRange(existingFolders);

            Action addRange = () => pack.AddRange(newFolders);
            var expectedMessage = string.Format(
                Strings.Argument_ElementDuplicatesExistingFolderNameHash,
                2);

            addRange.Should().Throw<ArgumentException>(
                "because 'folders' contains a duplicate of an existing folder")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        AddRange_ShouldThrowArgumentException_WhenFoldersInternallyRepeatsAnyNameHash()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var newFolders = new ISqPackFolder[] {
                new MockSqPackFolder(99),
                new MockSqPackFolder(4),
                new MockSqPackFolder(99),
            };

            Action addRange = () => pack.AddRange(newFolders);
            var expectedMessage = string.Format(
                Strings.Argument_ElementDuplicatesPreviousNameHash,
                2,
                0);

            addRange.Should().Throw<ArgumentException>(
                "because 'folders' contains multiple elements with the same 'NameHash' value")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void AddRange_ShouldUpdatePack_WhenSuccessful()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var folders = new ISqPackFolder[3];
            for (var i = 0; i < folders.Length; ++i)
            {
                folders[i] = new MockSqPackFolder(i);
            }
            pack.AddRange(folders);

            foreach (var folder in folders)
            {
                pack.Folders
                    .Should().Contain(
                        folder,
                        "because the folder was added to the 'Folders' property");
                pack.GetFolderByNameHash(folder.NameHash).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(folder, "because the name hash map was updated");
            }
        }

        // IMutableSqPack AsMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void AsMutable_ShouldReturnThis()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);

            object.ReferenceEquals(pack, pack.AsMutable())
                .Should().BeTrue(
                    "Because AsMutable is a cast.");
        }

        // Option<ISqPackFile> GetFileByPathNameHash(long, long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFileByPathNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeFolderNameHash(
            long folderNameHash)
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action getFileByPathNameHash = () =>
                pack.GetFileByPathNameHash(folderNameHash, 0);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFileByPathNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because folderNameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folderNameHash",
                    "because we fully describe our exceptions");
        }

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFileByPathNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeFileNameHash(
            long fileNameHash)
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action getFileByPathNameHash = () =>
                pack.GetFileByPathNameHash(0, fileNameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFileByPathNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because fileNameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "fileNameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        GetFileByPathNameHash_ShouldReturnOptionNone_WithNonPresentPathNameHash()
        {
            var files = new ISqPackFile[2][];
            for (int i = 0; i < 2; ++i)
            {
                files[i] = new ISqPackFile[2];
                for (int j = 0; j < 2; ++j)
                {
                    files[i][j] = new MockSqPackFile(i*2 + j);
                }
            }
            var folders = new ISqPackFolder[2];
            for (int i = 0; i < 2; ++i)
            {
                folders[i] = new SqPackFolder(i*100, files[i]);
            }
            var pack = new MutableSqPack(_rootDirectory, "", folders);

            pack.GetFileByPathNameHash(300, 0).IsNone
                .Should().BeTrue("because no such folder exists");

            pack.GetFileByPathNameHash(100, 5).IsNone
                .Should().BeTrue("because the folder exists, but not the file");
        }

        [Fact]
        public static void
        GetFileByPathNameHash_ShouldReturnExpectedFile_WithPresentPathNameHash()
        {
            var files = new ISqPackFile[2][];
            for (int i = 0; i < 2; ++i)
            {
                files[i] = new ISqPackFile[2];
                for (int j = 0; j < 2; ++j)
                {
                    files[i][j] = new MockSqPackFile(i*2 + j);
                }
            }
            var folders = new ISqPackFolder[2];
            for (int i = 0; i < 2; ++i)
            {
                folders[i] = new SqPackFolder(i*100, files[i]);
            }
            var pack = new MutableSqPack(_rootDirectory, "", folders);

            for (var i = 0; i < folders.Length; ++i)
            {
                for (var j = 0; j < files[i].Length; ++j)
                {
                    pack.GetFileByPathNameHash(i*100, i*2 + j).Match(
                        some: sv => sv,
                        none: nv => null)
                        .Should().Be(
                            files[i][j],
                            "because the path name hash map should be correct");
                }
            }
        }

        // void Remove(ISqPackFolder)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        Remove_ShouldThrowArgumentNullException_WithNullFolder()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action removeAction = () => pack.Remove(null);

            removeAction.Should().Throw<ArgumentNullException>(
                "because we cannot remove a null folder")
                .And.ParamName.Should().Be(
                    "folder",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Remove_ShouldThrowArgumentException_WithNonExistentFolder()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            Action removeAction = () => pack.Remove(new MockSqPackFolder());

            removeAction.Should().Throw<ArgumentException>(
                "because we cannot remove a folder that does not exist")
                .WithMessage(
                    $"{Strings.Argument_NonExistentFolder}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folder",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Remove_ShouldUpdatePack_WhenSuccessful()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var folders = new ISqPackFolder[] {
                new MockSqPackFolder(10),
                new MockSqPackFolder(20),
                new MockSqPackFolder(30),
            };
            var remainingFolders = new ISqPackFolder[] {
                folders[0],
                folders[2],
            };
            var removedFolder = folders[1];
            pack.AddRange(folders);
            pack.Remove(removedFolder);

            // This isn't a great "because" description, but BeEquivalentTo()
            // kills two tests with one stone:
            //    * Folders should not contain the removed folder.
            //    * Folders should contain all of the remaining folders, in their
            //      original order.
            pack.Folders.Should().BeEquivalentTo(
                remainingFolders,
                "because Remove() should correctly update the 'Folders' property");

            foreach (var remainingFolder in remainingFolders)
            {
                pack.GetFolderByNameHash(remainingFolder.NameHash).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(
                        remainingFolder,
                        "because Remove() should preserve remaining folder name hash map entries");
            }

            pack.GetFolderByNameHash(folders[1].NameHash).IsNone
                .Should().BeTrue(
                    "because Remove() removes the target folder's name hash map entry");
        }

        // IMutableSqPack ToMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToMutable_ShouldReturnDistinctButEquivalentMutableSqPack()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var mutablePack = pack.ToMutable();

            object.ReferenceEquals(pack, mutablePack)
                .Should().BeFalse("because ToMutable is a copy, not a cast");

            mutablePack.Directory.FullName
                .Should().Be(
                    pack.Directory.FullName,
                    "because ToMutable() produces an equivalent instance");

            mutablePack.FileNameRoot
                .Should().BeEquivalentTo(
                    pack.FileNameRoot,
                    "because ToMutable() produces an equivalent instance");

            mutablePack.Folders
                .Should().BeEquivalentTo(
                    pack.Folders,
                    "because ToMutable() produces an equivalent instance");
        }

        // ISqPack ToImmutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToImmutable_ShouldReturnDistinctButEquivalentSqPack()
        {
            var pack = new MutableSqPack(_rootDirectory, "", _emptyFolders);
            var immutablePack = pack.ToImmutable();

            object.ReferenceEquals(pack, immutablePack)
                .Should().BeFalse("because ToImmutable is a copy, not a cast");

            immutablePack.Directory.FullName
                .Should().Be(
                    pack.Directory.FullName,
                    "because ToImmutable() produces an equivalent instance");

            immutablePack.FileNameRoot
                .Should().BeEquivalentTo(
                    pack.FileNameRoot,
                    "because ToImmutable() produces an equivalent instance");

            immutablePack.Folders
                .Should().BeEquivalentTo(
                    pack.Folders,
                    "because ToImmutable() produces an equivalent instance");
        }
    }
}
