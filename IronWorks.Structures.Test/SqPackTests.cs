namespace IronWorks.Structures.Test
{
    using System;
    using System.Collections.Immutable;
    using System.IO;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;
    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPack"/>.
    /// </summary>
    public static class SqPackTests
    {
        private static ImmutableList<ISqPackFolder> _emptyFolders
            = new ISqPackFolder[0].ToImmutableList();

        private static DirectoryInfo _rootDirectory = new DirectoryInfo("/");

        // Tests
        // ========================================================================

        // SqPack(
        //     DirectoryInfo directory,
        //     string fileNameRoot,
        //     IEnumerable<ISqPackFolder> folders)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullDirectory()
        {
            Action ctor = () => new SqPack(null, "", _emptyFolders);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'directory' cannot be null")
                .And.ParamName.Should().Be(
                    "directory",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullFileNameRoot()
        {
            Action ctor = () => new SqPack(_rootDirectory, null, _emptyFolders);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'fileNameRoot' cannot be null")
                .And.ParamName.Should().Be(
                    "fileNameRoot",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullFolders()
        {
            Action ctor = () => new SqPack(_rootDirectory, "", null);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'folders' cannot be null")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentException_WithNullElementInFolders()
        {
            var folders = new ISqPackFolder[] {
                new MockSqPackFolder(0),
                null,
                new MockSqPackFolder(2),
            };
            Action ctor = () => new SqPack(_rootDirectory, "", folders);
            var expectedMessage = string.Format(
                Strings.Argument_NullElement, 1);

            ctor.Should().Throw<ArgumentException>(
                "because 'folders' cannot contain null references")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folders",
                    "because we fully describe our exceptions");
        }

        // SqPack(ISqPack other)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        CopyCtor_ShouldThrowArgumentNullException_WithNullOther()
        {
            Action ctor = () => new SqPack((ISqPack)null);

            ctor.Should().Throw<ArgumentNullException>(
                "because we cannot copy a null instance.")
                .And.ParamName.Should().Be(
                    "other",
                    "because we describe our exceptions");
        }

        // IsMutable property
        // ------------------------------------------------------------------------

        [Fact]
        public static void ISqPackIsMutable_ShouldBeFalse()
        {
            ISqPack pack = new SqPack(_rootDirectory, "", _emptyFolders);
            pack.IsMutable.Should().BeFalse();
        }

        // IMutableSqPack AsMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void AsMutable_ShouldThrowNotSupportedException()
        {
            ISqPack pack = new SqPack(_rootDirectory, "", _emptyFolders);
            Action asMutable = () => pack.AsMutable();

            asMutable.Should().Throw<NotSupportedException>(
                "because SqPack is immutable")
                .WithMessage(
                    Strings.NotSupported_Immutable,
                    "because we describe our exceptions");
        }

        // Option<ISqPackFolder> GetFolderByNameHash(long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFolderByNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            var pack = new SqPack(_rootDirectory, "", _emptyFolders);
            Action getFolderByNameHash = () =>
                pack.GetFolderByNameHash(nameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFolderByNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        GetFolderByNameHash_ShouldReturnOptionNone_WithNonPresentHash()
        {
            new SqPack(_rootDirectory,
                       "",
                       new MockSqPackFolder[] {
                           new MockSqPackFolder(0),
                           new MockSqPackFolder(1),
                       })
                .GetFolderByNameHash(5).IsNone
                .Should().BeTrue("because no such folder exists");
        }

        [Fact]
        public static void
        GetFolderByNameHash_ShouldReturnExpectedFolder_WithPresentHash()
        {
            var folders = new MockSqPackFolder[] {
                new MockSqPackFolder(0),
                new MockSqPackFolder(1),
            };
            var pack = new SqPack(_rootDirectory, "", folders);

            for (var i = 0; i < folders.Length; ++i)
            {
                pack.GetFolderByNameHash(i).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(
                        folders[i],
                        "because the name hash map should be correct");
            }
        }

        // Maybe<ISqPackFile> GetFileByPathNameHash(long, long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFileByPathNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeFolderNameHash(
            long folderNameHash)
        {
            var pack = new SqPack(_rootDirectory, "", _emptyFolders);
            Action getFileByPathNameHash = () =>
                pack.GetFileByPathNameHash(folderNameHash, 0);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFileByPathNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because folderNameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "folderNameHash",
                    "because we fully describe our exceptions");
        }

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFileByPathNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeFileNameHash(
            long fileNameHash)
        {
            var pack = new SqPack(_rootDirectory, "", _emptyFolders);
            Action getFileByPathNameHash = () =>
                pack.GetFileByPathNameHash(0, fileNameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFileByPathNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because fileNameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "fileNameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        GetFileByPathNameHash_ShouldReturnMaybeNothing_WithNonPresentPathNameHash()
        {
            var files = new ISqPackFile[2][];
            for (int i = 0; i < 2; ++i)
            {
                files[i] = new ISqPackFile[2];
                for (int j = 0; j < 2; ++j)
                {
                    files[i][j] = new MockSqPackFile(i*2 + j);
                }
            }
            var folders = new ISqPackFolder[2];
            for (int i = 0; i < 2; ++i)
            {
                folders[i] = new SqPackFolder(i*100, files[i]);
            }
            var pack = new SqPack(_rootDirectory, "", folders);

            pack.GetFileByPathNameHash(300, 0).IsNone
                .Should().BeTrue("because no such folder exists");
            pack.GetFileByPathNameHash(100, 5).IsNone
                .Should().BeTrue("because the folder exists, but not the file");
        }

        [Fact]
        public static void
        GetFileByPathNameHash_ShouldReturnExpectedFile_WithPresentPathNameHash()
        {
            var files = new ISqPackFile[2][];
            for (int i = 0; i < 2; ++i)
            {
                files[i] = new ISqPackFile[2];
                for (int j = 0; j < 2; ++j)
                {
                    files[i][j] = new MockSqPackFile(i*2 + j);
                }
            }
            var folders = new ISqPackFolder[2];
            for (int i = 0; i < 2; ++i)
            {
                folders[i] = new SqPackFolder(i*100, files[i]);
            }
            var pack = new SqPack(_rootDirectory, "", folders);

            for (var i = 0; i < folders.Length; ++i)
            {
                for (var j = 0; j < files[i].Length; ++j)
                {
                    pack.GetFileByPathNameHash(i*100, i*2 + j).Match(
                        some: sv => sv,
                        none: nv => null)
                        .Should().Be(
                            files[i][j],
                            "because the path name hash map should be correct");
                }
            }
        }

        // IMutableSqPackPack ToMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToMutable_ShouldReturnEquivalentMutableSqPackPack()
        {
            var pack = new SqPack(_rootDirectory, "", _emptyFolders);
            var mutablePack = pack.ToMutable();

            mutablePack.Directory.FullName
                .Should().Be(
                    pack.Directory.FullName,
                    "because ToMutable() produces an equivalent instance");

            mutablePack.FileNameRoot
                .Should().BeEquivalentTo(
                    pack.FileNameRoot,
                    "because ToMutable() produces an equivalent instance");

            mutablePack.Folders
                .Should().BeEquivalentTo(
                    pack.Folders,
                    "because ToMutable() produces an equivalent instance");
        }
    }
}
