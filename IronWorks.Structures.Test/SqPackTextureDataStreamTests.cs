namespace IronWorks.Structures.Test
{
    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackTextureDataStream"/>.
    /// </summary>
    public static class SqPackTextureDataStreamTests
    {
        // SqPackDataStreamType ContentType { get; }
        // ---------------------------------------------------------------------

        [Fact]
        public static void ContentType_ShouldBeSqPackDataStreamTypeTexture()
        {
            var stream = new SqPackTextureDataStream(
                8,
                new byte[4],
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock()
                });

            stream.ContentType.Should().Be(SqPackDataStreamType.Texture);
        }

        // string FileExtension { get; }
        // ---------------------------------------------------------------------

        [Fact]
        public static void FileExtension_ShouldBeEquivalentToTex()
        {
            var stream = new SqPackTextureDataStream(
                8,
                new byte[4],
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock()
                });

            stream.FileExtension.Should().BeEquivalentTo("tex");
        }
    }
}
