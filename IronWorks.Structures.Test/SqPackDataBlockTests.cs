namespace IronWorks.Structures.Test
{
    using System;
    using System.IO;
    using System.IO.Compression;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackDataBlock"/>.
    /// </summary>
    public static class SqPackDataBlockTests
    {
        private static readonly byte[] _emptyData = new byte[0];

        // public SqPackDataBlock(
        //     bool isCompressed,
        //     int uncompressedSize,
        //     IEnumerable<byte> data)
        // ---------------------------------------------------------------------

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeUncompressedSize()
        {
            Action ctor = () =>
                new SqPackDataBlock(false, -1, _emptyData);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because sizes can't be negative")
                .WithMessage(
                    $"{Strings.ArgumentOutOfRange_NegativeSize}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "uncompressedSize",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Ctor_ShouldThrowArgumentNullException_WithNullData()
        {
            Action ctor = () =>
                new SqPackDataBlock(false, 0, null);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'data' is a required parameter")
                .And.ParamName.Should().Be(
                    "data",
                    "because we describe our exceptions");
        }

        // IsMutable property
        // ---------------------------------------------------------------------

        [Fact]
        public static void IsMutable_ShouldBeFalse()
        {
            new SqPackDataBlock(false, 0, _emptyData)
                .IsMutable.Should().BeFalse();
        }

        // byte[] Unpack()
        // ---------------------------------------------------------------------

        [Fact]
        public static void Unpack_ShouldBeExpectedValue_WithUncompressedBlock()
        {
            var data = new byte[256];
            for (var i = 0; i < 256; ++i)
            {
                data[i] = (byte)i;
            }

            new SqPackDataBlock(false, 256, data)
                .Unpack().Should().BeEquivalentTo(data);
        }

        [Fact]
        public static void Unpack_ShouldBeExpectedValue_WithCompressedBlock()
        {
            var data = new byte[256];
            for (var i = 0; i < 256; ++i)
            {
                data[i] = (byte)i;
            }

            byte[] compressedData;
            using (var compressedDataStream = new MemoryStream())
            {
                using (var deflateStream = new DeflateStream(
                           compressedDataStream, CompressionMode.Compress))
                {
                    deflateStream.Write(data, 0, data.Length);
                }
                compressedData = compressedDataStream.ToArray();
            }

            new SqPackDataBlock(true, 256, compressedData)
                .Unpack().Should().BeEquivalentTo(data);
        }

        // void Unpack(Stream)
        // ---------------------------------------------------------------------


        [Fact]
        public static void UnpackStream_ShouldBeExpectedValue_WithUncompressedBlock()
        {
            var data = new byte[256];
            for (var i = 0; i < 256; ++i)
            {
                data[i] = (byte)i;
            }

            var outStream = new MemoryStream();
            new SqPackDataBlock(false, 256, data)
                .Unpack(outStream);
            outStream.ToArray().Should().BeEquivalentTo(data);
        }

        [Fact]
        public static void UnpackStream_ShouldBeExpectedValue_WithCompressedBlock()
        {
            var data = new byte[256];
            for (var i = 0; i < 256; ++i)
            {
                data[i] = (byte)i;
            }

            byte[] compressedData;
            using (var compressedDataStream = new MemoryStream())
            {
                using (var deflateStream = new DeflateStream(
                           compressedDataStream, CompressionMode.Compress))
                {
                    deflateStream.Write(data, 0, data.Length);
                }
                compressedData = compressedDataStream.ToArray();
            }

            var outStream = new MemoryStream();
            new SqPackDataBlock(true, 256, compressedData)
                .Unpack(outStream);
            outStream.ToArray().Should().BeEquivalentTo(data);
        }
    }
}
