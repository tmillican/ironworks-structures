namespace IronWorks.Structures.Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackDataStream"/>.
    /// </summary>
    public static class SqPackDataStreamTests
    {
        private static byte[] _emptyHeader = new byte[0];

        private static ISqPackDataBlock[] _emptyBlocks = new ISqPackDataBlock[0];

        // SqPackDataStream(
        //     int uncompressedSize,
        //     IEnumerable<byte> originalHeader,
        //     IEnumerable<ISqPackDataBlock> blocks)
        // ---------------------------------------------------------------------
        //
        // This exercises the two-argument ctor as well, via ctor chaining.

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeUncompressedSize()
        {
            Action ctor = () => new DummyStream(-1, _emptyHeader, _emptyBlocks);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because data sizes can't be negative")
                .WithMessage(
                    $"{Strings.ArgumentOutOfRange_NegativeSize}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "uncompressedSize",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullOriginalHeader()
        {
            Action ctor = () => new DummyStream(0, null, _emptyBlocks);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'originalHeader' is a required argument")
                .And.ParamName.Should().Be(
                    "originalHeader",
                    "because we describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentNullException_WithNullBlocks()
        {
            Action ctor = () => new DummyStream(0, _emptyHeader, null);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'blocks' is a required argument")
                .And.ParamName.Should().Be(
                    "blocks",
                    "because we describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentException_WithNullElementInBlocks()
        {
            Action ctor = () =>
                new DummyStream(
                    0,
                    _emptyHeader,
                    new ISqPackDataBlock[] {
                        new MockSqPackDataBlock(),
                        null,
                        new MockSqPackDataBlock()
                    });
            var expectedMessage = string.Format(Strings.Argument_NullElement, 1);


            ctor.Should().Throw<ArgumentException>(
                "because each element of 'blocks' must be an actual block")
                .WithMessage($"{expectedMessage}*")
                .And.ParamName.Should().Be(
                    "blocks",
                    "because we describe our exceptions");
        }

        // byte[] Unpack()
        // ---------------------------------------------------------------------

        [Fact]
        public static void Unpack_ShouldBeExpectedValue_WithHeader()
        {
            var expected = new byte[] {
                0xBA, 0xAD, 0xF0, 0x0D,
                0xDE, 0xAD, 0xBE, 0xEF,
                0xDE, 0xAD, 0xBE, 0xEF
            };
            var stream = new DummyStream(
                8,
                new byte[] {
                    0xBA, 0xAD, 0xF0, 0x0D,
                },
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock(),
                    new MockSqPackDataBlock()
                });

            stream.Unpack().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public static void Unpack_ShouldBeExpectedValue_WithoutHeader()
        {
            var expected = new byte[] {
                0xDE, 0xAD, 0xBE, 0xEF,
                0xDE, 0xAD, 0xBE, 0xEF
            };
            var stream = new DummyStream(
                8,
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock(),
                    new MockSqPackDataBlock()
                });

            stream.Unpack().Should().BeEquivalentTo(expected);
        }

        // byte Unpack(Stream)
        // ---------------------------------------------------------------------

        [Fact]
        public static void UnpackStream_ShouldBeExpectedValue_WithHeader()
        {
            var expected = new byte[] {
                0xBA, 0xAD, 0xF0, 0x0D,
                0xDE, 0xAD, 0xBE, 0xEF,
                0xDE, 0xAD, 0xBE, 0xEF
            };
            var cstream = new DummyStream(
                8,
                new byte[] {
                    0xBA, 0xAD, 0xF0, 0x0D,
                },
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock(),
                    new MockSqPackDataBlock()
                });

            var outStream = new MemoryStream();
            cstream.Unpack(outStream);
            outStream.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public static void UnpackStream_ShouldBeExpectedValue_WithoutHeader()
        {
            var expected = new byte[] {
                0xDE, 0xAD, 0xBE, 0xEF,
                0xDE, 0xAD, 0xBE, 0xEF
            };
            var cstream = new DummyStream(
                8,
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock(),
                    new MockSqPackDataBlock()
                });

            var outStream = new MemoryStream();
            cstream.Unpack(outStream);
            outStream.ToArray().Should().BeEquivalentTo(expected);
        }
    }

    /// <summary>
    ///   Dummy derived class for testing base class.
    /// </summary>
    internal class DummyStream : SqPackDataStream
    {
        public DummyStream(
            int uncompressedSize,
            IEnumerable<byte> originalHeader,
            IEnumerable<ISqPackDataBlock> blocks)
            : base(uncompressedSize, originalHeader, blocks)
        {
        }

        public DummyStream(
            int uncompressedSize,
            IEnumerable<ISqPackDataBlock> blocks)
            : base(uncompressedSize, blocks)
        {
        }
    }
}
