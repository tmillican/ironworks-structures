namespace IronWorks.Structures.Test
{
    using System;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="MutableSqPackFile"/>.
    /// </summary>
    public static class MutableSqPackFileTests
    {
        // Tests
        // ========================================================================

        // MutableSqPackFile(long, byte, long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            Action ctor = () => new MutableSqPackFile(nameHash, 0, 0);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeDataOffset()
        {
            Action ctor = () => new MutableSqPackFile(0, 0, -1);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because file offsets must be non-negative")
                .WithMessage($"{Strings.ArgumentOutOfRange_NegativeFileOffset}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "dataOffset",
                    "because we fully describe our exceptions");
        }

        // MutableSqPackFile(ISqPackFile)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        CopyCtor_ShouldThrowArgumentNullException_WithNullOther()
        {
            Action ctor = () => new MutableSqPackFile((ISqPackFile)null);

            ctor.Should().Throw<ArgumentNullException>(
                "because we cannot copy a null instance.")
                .And.ParamName.Should().Be(
                    "other",
                    "because we describe our exceptions");
        }

        // IsMutable property
        // ------------------------------------------------------------------------

        [Fact]
        public static void IsMutable_ShouldBeTrue()
        {
            new MutableSqPackFile(0, 0, 0).IsMutable.Should().BeTrue();
        }

        // DataOffset property
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        SetDataOffset_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeValue()
        {
            Action setDataOffset = () =>
                new MutableSqPackFile(0, 0, 0).DataOffset = -1;

            setDataOffset.Should().Throw<ArgumentOutOfRangeException>(
                "because file offsets must be non-negative")
                .WithMessage($"{Strings.ArgumentOutOfRange_NegativeFileOffset}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "value",
                    "because we fully describe our exceptions");
        }

        // NameHash property
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        SetNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long value)
        {
            Action setNameHash = () =>
                new MutableSqPackFile(0, 0, 0).NameHash = value;
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            setNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "value",
                    "because we fully describe our exceptions");
        }

        // IMutableSqPackFile AsMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void AsMutable_ShouldReturnThis()
        {
            var file = new MutableSqPackFile(0, 1, 2);
            var mutableFile = file.AsMutable();

            object.ReferenceEquals(file, mutableFile).Should().BeTrue();
        }

        // IMutableSqPackFile ToMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToMutable_ShouldReturnDistinctButEquivalentMutableSqPackFile()
        {
            var file = new MutableSqPackFile(0, 1, 2);
            var mutableFile = file.ToMutable();

            object.ReferenceEquals(file, mutableFile)
                .Should().BeFalse(
                    "because ToMutable() is a copy, not a cast.");

            mutableFile.NameHash
                .Should().Be(
                    file.NameHash,
                    "because ToMutable() produces an equivalent instance");

            mutableFile.DataVolumeId
                .Should().Be(
                    file.DataVolumeId,
                    "because ToMutable() produces an equivalent instance");

            mutableFile.DataOffset
                .Should().Be(
                    file.DataOffset,
                    "because ToMutable() produces an equivalent instance");
        }

        // ISqPackFile ToImmutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToImmutable_ShouldReturnDistinctButEquivalentSqPackFile()
        {
            var file = new MutableSqPackFile(0, 1, 2);
            var immutableFile = file.ToImmutable();

            object.ReferenceEquals(file, immutableFile)
                .Should().BeFalse(
                    "because ToImmutable() is a copy, not a cast.");

            immutableFile.NameHash
                .Should().Be(
                    file.NameHash,
                    "because ToImmutable() produces an equivalent instance");

            immutableFile.DataVolumeId
                .Should().Be(
                    file.DataVolumeId,
                    "because ToImmutable() produces an equivalent instance");

            immutableFile.DataOffset
                .Should().Be(
                    file.DataOffset,
                    "because ToImmutable() produces an equivalent instance");
        }
    }
}
