namespace IronWorks.Structures.Test
{
    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackBinaryDataStream"/>.
    /// </summary>
    public static class SqPackBinaryDataStreamTests
    {
        // SqPackDataStreamType ContentType { get; }
        // ---------------------------------------------------------------------

        [Fact]
        public static void ContentType_ShouldBeSqPackDataStreamTypeBinary()
        {
            var stream = new SqPackBinaryDataStream(
                4,
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock()
                });

            stream.ContentType.Should().Be(SqPackDataStreamType.Binary);
        }

        // string FileExtension { get; }
        // ---------------------------------------------------------------------

        [Fact]
        public static void FileExtension_ShouldBeEquivalentToBin()
        {
            var stream = new SqPackBinaryDataStream(
                4,
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock()
                });

            stream.FileExtension.Should().BeEquivalentTo("bin");
        }
    }
}
