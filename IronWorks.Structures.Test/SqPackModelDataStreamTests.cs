namespace IronWorks.Structures.Test
{
    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackModelDataStream"/>.
    /// </summary>
    public static class SqPackModelDataStreamTests
    {
        // SqPackDataStreamType ContentType { get; }
        // ---------------------------------------------------------------------

        [Fact]
        public static void ContentType_ShouldBeSqPackDataStreamTypeModel()
        {
            var stream = new SqPackModelDataStream(
                4,
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock()
                });

            stream.ContentType.Should().Be(SqPackDataStreamType.Model);
        }

        // string FileExtension { get; }
        // ---------------------------------------------------------------------

        [Fact]
        public static void FileExtension_ShouldBeEquivalentToMdl()
        {
            var stream = new SqPackModelDataStream(
                4,
                new ISqPackDataBlock[] {
                    new MockSqPackDataBlock()
                });

            stream.FileExtension.Should().BeEquivalentTo("mdl");
        }
    }
}
