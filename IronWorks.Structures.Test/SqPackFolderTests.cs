namespace IronWorks.Structures.Test
{
    using System;
    using System.Collections.Immutable;
    using System.Linq;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Monads;
    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackFolder"/>.
    /// </summary>
    public static class SqPackFolderTests
    {
        private static readonly ImmutableList<ISqPackFile> _emptyFiles
            = new ISqPackFile[0].ToImmutableList();

        // Tests
        // ========================================================================

        // SqPackFolder(long nameHash, IEnumerable<ISqPackFile> files)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            Action ctor =
                () => new SqPackFolder(nameHash, _emptyFiles);

            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Ctor_ShouldThrowArgumentNullException_WithNullFiles()
        {
            Action ctor = () => new SqPackFolder(0, null);

            ctor.Should().Throw<ArgumentNullException>(
                "because 'files' cannot be null")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void Ctor_ShouldThrowArgumentException_WithNullElementInFiles()
        {
            var files = new ISqPackFile[3] {
                new MockSqPackFile(0),
                null,
                new MockSqPackFile(2),
            };
            Action ctor = () => new SqPackFolder(0, files);
            var expectedMessage = string.Format(
                Strings.Argument_NullElement, 1);

            ctor.Should().Throw<ArgumentException>(
                "because 'files' cannot contain null references")
                .WithMessage(
                    $"{expectedMessage}*",
                    "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "files",
                    "because we fully describe our exceptions");
        }

        // SqPackFolder(ISqPackFolder other)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        CopyCtor_ShouldThrowArgumentNullException_WithNullOther()
        {
            Action ctor = () => new SqPackFolder((ISqPackFolder)null);

            ctor.Should().Throw<ArgumentNullException>(
                "because we cannot copy a null instance.")
                .And.ParamName.Should().Be(
                    "other",
                    "because we describe our exceptions");
        }

        // IsMutable property
        // ------------------------------------------------------------------------

        [Fact]
        public static void IsMutable_ShouldBeFalse()
        {
            ISqPackFolder folder = new SqPackFolder(0, _emptyFiles);
            folder.IsMutable
                .Should().BeFalse("because SqPackFolder is immutable");
        }

        // IMutableSqPackFolder AsMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void AsMutable_ShouldThrowNotSupportedException()
        {
            var folder = new SqPackFolder(0, _emptyFiles);
            Action asMutable = () => folder.AsMutable();

            asMutable.Should().Throw<NotSupportedException>(
                "because SqPackFolder is immutable")
                .WithMessage(
                    Strings.NotSupported_Immutable,
                    "because we describe our exceptions");
        }

        // Option<ISqPackFile> GetFileByNameHash(long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        GetFileByNameHash_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            var folder = new SqPackFolder(0, _emptyFiles);
            Action getFileByNameHash = () =>
                folder.GetFileByNameHash(nameHash);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            getFileByNameHash.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void GetFileByNameHash_ShouldReturnOptionNone_WithNonPresentHash()
        {
            new SqPackFolder(0,
                       new MockSqPackFile[] {
                           new MockSqPackFile(0),
                           new MockSqPackFile(1),
                       })
                .GetFileByNameHash(5).IsNone
                .Should().BeTrue("because no such file exists");
        }

        [Fact]
        public static void GetFileByNameHash_ShouldReturnExpectedFile_WithPresentHash()
        {
            var files = new MockSqPackFile[] {
                new MockSqPackFile(0),
                new MockSqPackFile(1),
            };
            var folder = new SqPackFolder(0, files);

            for (var i = 0; i < files.Length; ++i)
            {
                folder.GetFileByNameHash(i).Match(
                    some: sv => sv,
                    none: nv => null)
                    .Should().Be(
                        files[i],
                        "because the name hash map should be correct");
            }
        }

        // IMutableSqPackFolder ToMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToMutable_ShouldReturnEquivalentMutableSqPackFolder()
        {
            var folder = new SqPackFolder(0, _emptyFiles);
            var mutableFolder = folder.ToMutable();

            mutableFolder.NameHash
                .Should().Be(
                    folder.NameHash,
                    "because ToMutable() produces an equivalent instance");

            mutableFolder.Files
                .Should().BeEquivalentTo(
                    folder.Files,
                    "because ToMutable() produces an equivalent instance");
        }
    }
}
