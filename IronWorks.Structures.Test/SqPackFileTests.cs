namespace IronWorks.Structures.Test
{
    using System;

    using FluentAssertions;
    using Xunit;

    using IronWorks.Structures;

    /// <summary>
    ///   Tests for <see cref="SqPackFile"/>.
    /// </summary>
    public static class SqPackFileTests
    {
        // Tests
        // ========================================================================

        // SqPackFile(long, byte, long)
        // ------------------------------------------------------------------------

        [Theory]
        [InlineData(-1)]
        [InlineData((long)uint.MaxValue + 1)]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeNameHash(
            long nameHash)
        {
            Action ctor = () => new SqPackFile(nameHash, 0, 0);
            var expectedMessage = string.Format(
                Strings.ArgumentOutOfRange_Range,
                0,
                uint.MaxValue);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because nameHash is a CRC-32 value")
                .WithMessage($"{expectedMessage}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "nameHash",
                    "because we fully describe our exceptions");
        }

        [Fact]
        public static void
        Ctor_ShouldThrowArgumentOutOfRangeException_WithOutOfRangeDataOffset()
        {
            Action ctor = () => new SqPackFile(0, 0, -1);

            ctor.Should().Throw<ArgumentOutOfRangeException>(
                "because file offsets must be non-negative")
                .WithMessage($"{Strings.ArgumentOutOfRange_NegativeFileOffset}*",
                             "because we describe our exceptions")
                .And.ParamName.Should().Be(
                    "dataOffset",
                    "because we fully describe our exceptions");
        }

        // SqPackFile(ISqPackFile)
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        CopyCtor_ShouldThrowArgumentNullException_WithNullOther()
        {
            Action ctor = () => new SqPackFile((ISqPackFile)null);

            ctor.Should().Throw<ArgumentNullException>(
                "because we cannot copy a null instance.")
                .And.ParamName.Should().Be(
                    "other",
                    "because we describe our exceptions");
        }

        // IsMutable property
        // ------------------------------------------------------------------------

        [Fact]
        public static void IsMutable_ShouldBeFalse()
        {
            var file = new SqPackFile(0, 0, 0);
            file.IsMutable
                .Should().BeFalse("because SqPackFile is immutable");
        }

        // IMutableSqPackFile ToMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void
        ToMutable_ShouldReturnEquivalentMutableSqPackFile()
        {
            var file = new SqPackFile(0, 1, 2);
            var mutableFile = file.ToMutable();

            mutableFile.NameHash
                .Should().Be(
                    file.NameHash,
                    "because ToMutable() produces an equivalent instance");

            mutableFile.DataVolumeId
                .Should().Be(
                    file.DataVolumeId,
                    "because ToMutable() produces an equivalent instance");

            mutableFile.DataOffset
                .Should().Be(
                    file.DataOffset,
                    "because ToMutable() produces an equivalent instance");
        }

        // IMutableSqPackFile AsMutable()
        // ------------------------------------------------------------------------

        [Fact]
        public static void AsMutable_ShouldThrowNotSupportedException()
        {
            var file = new SqPackFile(0, 1, 2);
            Action asMutable = () => file.AsMutable();

            asMutable.Should().Throw<NotSupportedException>(
                "because SqPackFile is immutable")
                .WithMessage(
                    Strings.NotSupported_Immutable,
                    "because we describe our exceptions");
        }
    }
}
